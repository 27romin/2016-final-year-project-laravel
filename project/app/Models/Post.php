<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;

/**
 * Class Post
 * @package App\Models
 */
class Post extends Model
{

    /**
     * @var string
     */
    protected $table = 'post';
    /**
     * @var array
     */
    protected $guarded = array();

    /**
     * @var array
     */
    public static $rules = array(
        'title' => 'required',
        'body' => 'required',
        'slug' => 'unique:posts'
    );

    /**
     * A post belongs to a user
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    /**
     * A post user belongs to a group
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function group()
    {
        return $this->belongsTo('App\Models\Group', 'group_id');
    }

    /**
     * A post has many replies
     * @return mixed
     */
    public function replies()
    {
        return $this->hasMany('App\Models\Reply')->orderBy('created_at', 'ASC');
    }

    /**
     * Aggretion relationship, post belongs to forum and only forum
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function forum()
    {
        return $this->belongsTo('App\Models\Forum', 'forum_id');
    }

    /**
     * A post has many followers (Pinners)
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function followers()
    {
        return $this->belongsToMany('App\Models\User', 'following', 'post_id', 'user_id');
    }

}
