<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

/**
 * Class User
 * @package App\Models
 */
class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    /**
     * Attributes of the user table
     * @var string
     */
    protected $table = 'user';
    /**
     * @var array
     */
    protected $fillable = ['name', 'email', 'password'];
    /**
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * Create an association between group and user by foreign key
     * @return BelongsTo: Group
     */
    public function group()
    {
        return $this->belongsTo('App\Models\Group', 'group_id');
    }

    /**
     * A user follows many posts
     * @return mixed
     */
    public function following()
    {
        return $this->belongsToMany('App\Models\Post', 'following', 'user_id', 'post_id')->orderBy('created_at');;
    }

    /**
     * A user has many posts
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function post()
    {
        return $this->hasMany('App\Models\Post');
    }

}
