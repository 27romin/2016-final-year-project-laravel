<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Post;

/**
 * Class Category
 * @package App\Models
 */
class Category extends Model
{

    /**
     * @var string
     */
    protected $table = 'category';
    /**
     * @var array
     */
    protected $guarded = array();

    /**
     * @var array
     */
    public static $rules = array(
    );

    /**
     * A category has many forums
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function forum()
    {
        return $this->hasMany('App\Models\Forum');
    }
}
