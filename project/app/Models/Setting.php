<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Post;

/**
 * Class Setting
 * @package App\Models
 */
class Setting extends Model
{

    /**
     * @var string
     */
    protected $table = 'setting';
    /**
     * @var array
     */
    protected $guarded = array();

    /**
     * @var array
     */
    public static $rules = array(
    );
}
