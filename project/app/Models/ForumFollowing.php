<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Post;
use App\Models\Feed;

/**
 * Class ForumFollowing
 * @package App\Models
 */
class ForumFollowing extends Model
{

    /**
     * @var string
     */
    protected $table = 'forum_following';
    /**
     * @var array
     */
    protected $guarded = array();

    /**
     * @var array
     */
    public static $rules = array(
    );

    /**
     * The following of a forum is directly linked with many posts
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function post()
    {
        return $this->hasMany('App\Models\Post', 'forum_id');
    }
}
