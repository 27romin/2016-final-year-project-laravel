<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;

/**
 * Class Feed
 * @package App\Models
 */
class Feed extends Model
{

    /**
     * @var string
     */
    protected $table = 'feed';
    /**
     * @var array
     */
    protected $guarded = array();

    /**
     * @var array
     */
    public static $rules = array(
    );

    /**
     * Feeds have many posts
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function post()
    {
        return $this->hasMany('App\Models\Post', 'id', 'post_id');
    }
}
