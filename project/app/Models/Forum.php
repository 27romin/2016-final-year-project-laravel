<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Post;
use App\Models\Following;
use App\Models\ForumFollowing;

/**
 * Class Forum
 * @package App\Models
 */
class Forum extends Model
{

    /**
     * @var string
     */
    protected $table = 'forum';
    /**
     * @var array
     */
    protected $guarded = array();

    /**
     * @var array
     */
    public static $rules = array(
        'name' => 'required',
        'slug' => 'unique:posts'
    );

    /**
     * A forum (Topic) belongs a category
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo('App\Models\Category', 'category_id');
    }

    /**
     * A forum has many posts
     * @return mixed
     */
    public function posts()
    {
        return $this->hasMany('App\Models\Post')->orderBy('created_at', 'DESC');
    }

    /**
     * A forum is made by a user
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    /**
     * A forum has many replies (Count)
     * @return mixed
     */
    public function repliesCount()
    {
        return $this->hasMany('App\Models\Reply')->orderBy('created_at', 'DESC')->count();
    }

    /**
     * A forum has many followers
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function forumFollowers()
    {
        return $this->hasMany('App\Models\ForumFollowing');
    }


}
