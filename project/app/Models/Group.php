<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;

/**
 * Class Group
 * @package App\Models
 */
class Group extends Model
{
    /**
     * @var string
     */
    protected $table = 'group';
    /**
     * @var array
     */
    protected $guarded = array();

    /**
     * The user group of user
     * @var int
     */
    private $userGroup = 0;

    /**
     * The user group of an admin
     * @var int
     */
    private $adminGroup = 1;


    /**
     * Return users groups
     * @return int
     */
    public function getUserGroup()
    {
        return $this->userGroup;
    }


    /**
     * Return admins group
     * @return int
     */
    public function getAdminGroup()
    {
        return $this->adminGroup;
    }



}
