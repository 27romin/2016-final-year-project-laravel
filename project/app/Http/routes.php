<?php

/**
 * Routes for users & guests.
 */
Route::get('/', 'UserController@userFeeds');
Route::get('home', 'UserController@userFeeds');
Route::get('login', 'UserController@getLogin');
Route::get('register', 'UserController@getRegister');

/**
 * Routes for posts and forums
 */
Route::any("forum/{slug}", array(
    "as" => "home",
    "uses" => "ForumController@index"
));

Route::any("post/{slug}", array(
    "as" => "home",
    "uses" => "PostController@index"
));

/**
 * Routes after logged in.
 */

Route::get('profile/editpassword', 'UserController@getEditPassword');

Route::group(array('after' => 'auth'), function () {

    /**
     * Get user profile methods (Get Methods)
     */

    Route::get('profile/editprofile', 'UserController@getEditProfile');
    Route::get('profile/avatar', 'UserController@getUploadAvatar');
    Route::get('profile/cover', 'UserController@getUploadCover');
    Route::get('pinned', 'UserController@postFollowing');
    Route::get('logout', 'UserController@logout');

    /**
     * Get forum methods (Get Methods)
     */
    Route::get('managetopic/avatar/{slug}', 'ForumController@getUploadForumAvatar');
    Route::get('managetopic/cover/{slug}', 'ForumController@getUploadForumCover');
    Route::get('managetopic/settings/{slug}', 'ForumController@editTopic');
    Route::get('deletetopic/delete/{slug}', 'ForumController@deleteTopic');
    Route::get('topic/create', 'ForumController@getCreateTopic');
    Route::get('topic/manage', 'ForumController@getManageTopic');

    /**
     * Post user profile methods (Post Methods)
     */
    Route::post('updatepassword', 'UserController@updatePassword');
    Route::post('updateprofile', 'UserController@updateProfile');
    Route::post('updateavatar', 'UserController@updateAvatar');
    Route::post('updatecover', 'UserController@updateCover');
    Route::post('register', 'UserController@postRegister');
    Route::post('login', 'UserController@postLogin');

    /**
     * Forum Following methods (Resource Routes)
     */
    Route::resource('followtopic', 'ForumFollowController@store');
    Route::resource('unfollowtopic', 'ForumFollowController@destroy');

    /**
     * Methods for pinning posts (Post Methods)
     */
    Route::post('unfollow', 'FollowController@destroy');
    Route::post('follow', 'FollowController@store');

    /**
     * Post forum (Post Methods)
     */
    Route::post('updateforumavatar', 'ForumController@updateForumAvatar');
    Route::post('updateforumcover', 'ForumController@updateForumCover');
    Route::post('createtopic', 'ForumController@createTopic');
    Route::post('updatetopic', 'ForumController@updateTopic');

    /**
     * Post posts (Post Methods)
     */
    Route::post('submitpost', 'PostController@store');
    Route::post('updatepost', 'PostController@update');
    Route::post('deletepost', 'PostController@destroy');

    /**
     * Post reply (Post Methods)
     */
    Route::post('postcomment', 'ReplyController@store');
    Route::post('updatecomment', 'ReplyController@update');
    Route::post('deletecomment', 'ReplyController@destroy');


    /**
     * Get admin home (Get Methods)
     */
    Route::get('admin', 'AdminController@index');
    Route::get('admin/home', 'AdminController@home');

    /**
     * Get admin manage posts (Get Methods)
     */
    Route::get('admin/manageposts', 'AdminController@managePosts');
    Route::get('admin/editpost/{id}', 'AdminController@editPost');
    Route::get('admin_deletepost/{id}', 'AdminController@deletePost');

    /**
     * Post admin update posts (Post Methods)
     */
    Route::post('admin_updatepost', 'AdminController@updatePost');

    /**
     * Get admin manage topics (Get Methods)
     */
    Route::get('admin/createtopic', 'AdminController@createTopic');
    Route::get('admin/managetopics', 'AdminController@manageTopics');
    Route::get('admin/topicedit/{id}', 'AdminController@editTopic');
    Route::get('admin_deletetopic/{id}', 'AdminController@deleteTopic');

    /**
     * Get admin manage users (Get Methods)
     */
    Route::get('admin/manageusers', 'AdminController@manageUsers');
    Route::get('admin/createuser', 'AdminController@createUser');
    Route::get('admin/userroles', 'AdminController@assignRole');
    Route::get('admin_deleteuser/{id}', 'AdminController@deleteUser');
    Route::get('admin/edituser/{id}', 'AdminController@editUser');

    /**
     * Post admin manage users (Post Methods)
     */
    Route::post('admin_updatuser', 'AdminController@updateUser');
    Route::post('admin/postcreateuser', 'AdminController@insertUser');
    Route::post('admin_updatuserrole', 'AdminController@updateUserRole');

    /**
     * Admin Settings
     */
    Route::get('admin/settings', 'AdminController@settings');
    Route::post('admin_updatesettings', 'AdminController@updateAdminSettings');

    /**
     * Forum and post (Resource Routes)
     */
    Route::resource('forum', 'ForumController');
    Route::resource('post', 'PostController');

//    Route::resource('follow', 'FollowController');
//    Route::get('topiclist', 'ForumController@listTopics');
});


