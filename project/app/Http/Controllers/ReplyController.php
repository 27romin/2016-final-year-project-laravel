<?php namespace App\Http\Controllers;

use App\Models\Forum;
use App\Models\Post;
use App\Models\Reply;
use Request;
use Illuminate\Support\Str;
use Input, Validator, Auth;
use Carbon\Carbon;

/**
 * Class ReplyController
 * @package App\Http\Controllers
 */
class ReplyController extends Controller
{

    /**
     * Check if not banned or guest, validate the existance of the forum
     * and post. Save the reply if the forum and post both exist.
     * @return $this
     */
    public function store()
    {
        if(Auth::user()->group_id != 3) {
            if (!Auth::guest()) {
                $validate_post = Post::where('id', Input::get('post_id'))->first();
                $validate_forum = Forum::where('id', Input::get('forum_id'))->first();

                if ($validate_post || $validate_forum) {
                    $input = Input::all();
                    $v = Validator::make($input, Reply::$rules);
                    if ($v->passes()) {
                        $validated = Post::where('id', Input::get('post_id'))->first();
                        if ($validated) {
                            $reply = new Reply;
                            $reply->body = Input::get('body');
                            $reply->forum_id = Input::get('forum_id');
                            $reply->post_id = Input::get('post_id');
                            $reply->user_id = Auth::user()->id;
                            $reply->save();
                        }
                    }
                    return view('main.post.reply_buffer')->with('reply', $reply);
                }
            }
        }
    }

    /**
     * Check if not banned or guest, remove the post if original poster
     * or an administrator.
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy()
    {
        if(Auth::user()->group_id != 3) {
            if (!Auth::guest()) {
                $userController = new UserController();
                if ($userController->isAdmin()) {
                    Reply::where('id', Input::get('comment_id'))->where('post_id', Input::get('post_id'))->delete();
                } elseif (Input::get('user_id') == Auth::user()->id) {
                    Reply::where('id', Input::get('comment_id'))->where('post_id', Input::get('post_id'))->where('user_id', Auth::user()->id)->delete();
                    return redirect()->back();
                }
            }
        }
    }

    /**
     * Check if not banned or guest, update the post if original poster
     * or an administrator.
     */
    public function update(){
        if(Auth::user()->group_id != 3) {
            $validate = Reply::where('id', Input::get('comment_id'))->where('user_id', Auth::user()->id)->first();
            $userController = new UserController();
            if ($validate || $userController->isAdmin()) {
                Reply::where('id', Input::get('comment_id'))->update(
                    array(
                        'body' => Input::get('post_value'),
                    )
                );
            }
        }
    }


}