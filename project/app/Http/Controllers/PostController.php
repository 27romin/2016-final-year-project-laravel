<?php namespace App\Http\Controllers;

use App\Models\Forum;
use App\Models\Following;
use App\Models\Feed;
use App\Models\Post;
use App\Models\Reply;
use Request;
use Illuminate\Support\Str;
use Input, Validator, Auth;
use Carbon\Carbon;
use View;

/**
 * Class PostController
 * @package App\Http\Controllers
 */
class PostController extends Controller
{
    /**
     * Render posts according to is given id, collect all of its data,
     * check if logged in and render its page with all of its data passed.
     * @param $id
     * @return mixed
     */
    public function index($id)
    {
        $post = Post::where('id', $id)->first();
        $forums = Forum::orderBy('created_at')->get();
        $userController = new UserController();
        $followController = new FollowController();

        if (!Auth::guest()) {
            $checkFollowing = $followController->checkFollowing(Auth::user()->id);
        }
        $checkFollowing[] = NULL;
        $isAdmin = $userController->isAdmin();

        return View::make('main.post.post_single')
            ->with('checkFollowing', $checkFollowing)
            ->with('isAdmin', $isAdmin)
            ->with('forums', $forums)
            ->with('post', $post);
    }


    /**
     * Check if logged in, validate all inputs, check for its existance.
     * If a post exist, then randomize the posts title. Otherwise,
     * save the post and save a record for user feeds.
     * @return $this
     */
    public function store()
    {
        if (!Auth::guest()) {
            $input = Input::all();
            $v = Validator::make($input, Post::$rules);
            if ($v->passes()) {
                $validate = Post::where('title', Input::get('title'))->first();
                $post = new Post;
                $post->title = Input::get('title');
                $post->body = Input::get('body');

                if ($validate) {
                    $post->slug = Str::slug(Input::get('title') . '_' . str_random(3));
                } else {
                    $post->slug = Str::slug(Input::get('title'));
                }

                $post->user_id = Auth::user()->id;
                $post->forum_id = Input::get('forum_id');
                $post->save();

                $Feed = new Feed();
                $Feed->post_id = $post->id;
                $Feed->forum_id = $post->forum_id;
                $Feed->save();

            }
            return redirect()->back()->withErrors($v);
        }
    }

    /**
     * Check if logged in, validate its existance and its orignal poster.
     * Give rights to update if admin or the original poster and update each
     * field within table.
     */
    public function update()
    {
        if (!Auth::guest()) {
            $validate = Post::where('id', Input::get('post_id'))->where('user_id', Auth::user()->id)->first();
            $userController = new UserController();
            if ($validate || $userController->isAdmin()) {
                Post::where('id', Input::get('post_id'))->update(
                    array(
                        'title' => Input::get('post_title'),
                        'body' => Input::get('post_value'),
                        'slug' => Input::get('post_title'),
                    )
                );
            }
        }
    }

    /**
     * Check if logged in, validate the posts existance.
     * Check if admin or the original poster and remove the record accoring to
     * user rights.
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy()
    {
        if (!Auth::guest()) {
            $validate = Post::where('id', Input::get('post_id'))->where('user_id', Auth::user()->id)->first();
            $userController = new UserController();
            if (Input::get('user_id') == Auth::user()->id) {
                if ($validate || $userController->isAdmin()) {
                    Post::find(Input::get('post_id'))->delete();
                    Following::where('post_id', Input::get('post_id'))->delete();
                    Reply::where('post_id', Input::get('post_id'))->delete();
                    Feed::where('post_id', Input::get('post_id'))->delete();
                    return redirect()->back();
                }
            }
        }
    }

    /**
     * Simple BB code method that is escaped. Used with
     * Regex method in javascript, for posting, updating comments.
     * @param $text
     * @return mixed
     */
    public function showBBcodes($text)
    {
        $text = nl2br(e($text));
        $find = array(
            '~\[b\](.*?)\[/b\]~s',
            '~\[i\](.*?)\[/i\]~s',
            '~\[u\](.*?)\[/u\]~s',
            '~\[quote\](.*?)\[/quote\]~s',
            '~\[size=(.*?)\](.*?)\[/size\]~s',
            '~\[color=(.*?)\](.*?)\[/color\]~s',
            '~\[url\]((?:ftp|https?)://.*?)\[/url\]~s',
            '~\[img\](https?://.*?\.(?:jpg|jpeg|gif|png|bmp))\[/img\]~s',
            "/( |^);P( |$)/",
            "/( |^):P( |$)/",
            "/( |^):D( |$)/",
            "/( |^)KD( |$)/",
            "/( |^)XC( |$)/",
            "/( |^)TT_TT( |$)/",
            "/( |^):inlove( |$)/",
            "/( |^):sungrassface( |$)/",
            "/( |^):confused( |$)/",
            "/( |^):smailface( |$)/",
            "/( |^):facesmail( |$)/",
            "/( |^):smillingeye( |$)/",
            "/( |^):halo( |$)/",
            "/( |^):woo( |$)/",
        );

        $replace = array(
            '<b>$1</b>',
            '<i>$1</i>',
            '<span style="text-decoration:underline;">$1</span>',
            '<pre>$1</' . 'pre>',
            '<span style="font-size:$1px;">$2</span>',
            '<span style="color:$1;">$2</span>',
            '<a href="$1">$1</a>',
            '<img width="100%" src="$1" alt="" />',
            '<img width="25px" src="'.emoticons.'hello.png" alt="" />',
            '<img width="25px" src="'.emoticons.'smaillike.png" alt="" />',
            '<img width="25px" src="'.emoticons.'smallopen.png" alt="" />',
            '<img width="25px" src="'.emoticons.'smailyes.png" alt="" />',
            '<img width="25px" src="'.emoticons.'bandege.png" alt="" />',
            '<img width="25px" src="'.emoticons.'crying.png" alt="" />',
            '<img width="25px" src="'.emoticons.'love.png" alt="" />',
            '<img width="25px" src="'.emoticons.'sungrassface.png" alt="" />',
            '<img width="25px" src="'.emoticons.'confused.png" alt="" />',
            '<img width="25px" src="'.emoticons.'smailface.png" alt="" />',
            '<img width="25px" src="'.emoticons.'facesmail.png" alt="" />',
            '<img width="25px" src="'.emoticons.'smillingeye.png" alt="" />',
            '<img width="25px" src="'.emoticons.'halo.png" alt="" />',
            '<img width="25px" src="'.emoticons.'woo.png" alt="" />',
        );

        return preg_replace($find, $replace, $text);
    }


}