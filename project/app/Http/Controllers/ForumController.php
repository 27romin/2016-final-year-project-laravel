<?php namespace App\Http\Controllers;

use App\Models\Forum;
use App\Models\Following;
use App\Models\ForumFollowing;
use App\Models\Reply;
use App\Models\Post;
use Illuminate\Support\Str;
use Input, Validator, Auth;
use Carbon\Carbon;
use App\Http\Controllers\UserController as UserController;
use View, Response, Request, Redirect, DB, Image;

/**
 * Class ForumController (Aka: Topics)
 * @package App\Http\Controllers
 */
class ForumController extends Controller
{

    /**
     * Check if logged in and render create a topic page
     * @return View
     */
    public function getCreateTopic()
    {
        if(!Auth::guest()) {
            return view('main.user.create_topic');
        }
    }

    /**
     * Check if logged in and render manage a topic page
     * with its given data
     * @return mixed
     */
    public function getManageTopic()
    {
        if(!Auth::guest()) {
            $forums = Forum::where('user_id', Auth::user()->id)->get();
            return view('main.user.manage_topic')->with('forums', $forums);
        }
    }

    /**
     * Check if logged in, gather the data of the forum, check
     * for its existence and render edit a topic page
     * @param $slug
     * @return mixed
     */
    public function editTopic($slug)
    {
        if(!Auth::guest()) {
            $forum = Forum::where('slug', $slug)->where('user_id', Auth::user()->id)->first();
            if (is_null($forum)) {
                return redirect()->back()->withErrors('This topic does not exist');
            }
            return view('main.user.edit_topic')->with('forum', $forum);
        }
    }

    /**
     * Check if logged in, check for the topics existence and
     * render upload forum avatar page
     * @param $slug
     * @return mixed
     */
    public function getUploadForumAvatar($slug)
    {
        if(!Auth::guest()) {
            $forum = Forum::where('slug', $slug)->where('user_id', Auth::user()->id)->first();
            if (is_null($forum)) {
                return redirect()->back()->withErrors('This topic does not exist');
            }
            return View::make('main.user.upload_forum_avatar')->with('forum', $forum);
        }
    }

    /**
     * Check if logged in, check for the topics existence and
     * render upload forum cover page
     * @param $slug
     * @return mixed
     */
    public function getUploadForumCover($slug)
    {
        if(!Auth::guest()) {
            $forum = Forum::where('slug', $slug)->where('user_id', Auth::user()->id)->first();
            if (is_null($forum)) {
                return redirect()->back()->withErrors('This topic does not exist');
            }
            return View::make('main.user.upload_forum_cover')->with('forum', $forum);
        }
    }

    /**
     * A compnent of the view, get the records of 5
     * different topics
     * @return mixed
     */
    public function component_suggest(){
        return Forum::orderByRaw("RAND()")->take(5)->get();
    }

    /**
     * Render topic according to is given slug, check for its existence,
     * check all the followers and pinners of the topic and render all that
     * data with the page
     * @param $slug
     * @return mixed
     */
    public function index($slug)
    {
        $forums = $this->component_suggest();
        $forum = Forum::where('slug', $slug)->first();

        if (is_null($forum)) {
            return redirect()->back();
        }

        $forumFollowController = new ForumFollowController();
        $followController = new FollowController();
        $UserController = new UserController();
        $postController= new PostController();

        if (!Auth::guest()) {
            $checkForumFollowing = $forumFollowController->checkForumFollowing(Auth::user()->id);
            $checkFollowing = $followController->checkFollowing(Auth::user()->id);
        }

        $checkForumFollowing[] = NULL;
        $checkFollowing[] = NULL;

        $isAdmin = $UserController->isAdmin();
        $bbcode = $postController;

        return View::make('main.topic.topic')
            ->with('checkForumFollowing', $checkForumFollowing)
            ->with('checkFollowing', $checkFollowing)
            ->with('isAdmin', $isAdmin)
            ->with('forums', $forums)
            ->with('forum', $forum);
    }

    /**
     * Check if not banned or guest, validate all inputs,
     * save the image file once resized and update its path. Once
     * complete, redirect.
     * @return mixed
     */
    public function updateForumCover()
    {
        if(Auth::user()->group_id != 3) {
            if (!Auth::guest()) {
                $input = Input::all();
                $rules = array('image' => 'required');
                $v = Validator::make($input, $rules);
                if ($v->passes()) {
                    $image = Input::file('image');
                    Image::make($image)->fit(552, 166)->save();
                    $filename = str_random(11) . $image->getClientOriginalName();
                    Forum::where('user_id', Auth::user()->id)->where('id', Input::get('forum_id'))->update(
                        array(
                            'cover' => $filename,
                        )
                    );
                    $image->move(
                        base_path() . '/public/images/forumcoverupload/', $filename
                    );
                    return redirect()->back();
                } else {
                    return redirect()->back()->withErrors($v);
                }
            }
        }else{
            'You are banned';
        }
    }

    /**
     * Check if not banned or guest, validate all inputs,
     * save the image file once resized and update its path. Once
     * complete, redirect.
     * @return string
     */
    public function updateForumAvatar()
    {
        if(Auth::user()->group_id != 3) {
            if (!Auth::guest()) {
                $input = Input::all();
                $rules = array('image' => 'required');
                $v = Validator::make($input, $rules);
                if ($v->passes()) {
                    $image = Input::file('image');
                    $filename = str_random(11) . $image->getClientOriginalName();
                    Forum::where('user_id', Auth::user()->id)->where('id', Input::get('forum_id'))->update(
                        array(
                            'avatar' => $filename,
                        )
                    );
                    $image->move(
                        base_path() . '/public/images/forumavatarupload/', $filename
                    );
                    return redirect()->back();
                } else {
                    return redirect()->back()->withErrors($v);
                }
            }
        }else{
            return 'You are banned';
        }
    }

    /**
     * Check if not banned or guest, validate all inputs,
     * save the record within the table and redirect.
     * @return string
     */
    public function createTopic()
    {
        if(Auth::user()->group_id != 3) {
            if (!Auth::guest()) {
                $input = Input::all();
                $rules = array(
                    'name' => 'required|max:15|min:4',
                    'description' => 'required|max:250|min:10',
                    'address' => 'required|min:4',
                    'page_category' => 'required');
                $v = Validator::make($input, $rules);
                if ($v->passes()) {
                    $validate = Forum::where('slug', Input::get('address'))->first();
                    if (!$validate) {
                        $forum = new Forum;
                        $forum->name = Input::get('name');
                        $forum->user_id = Auth::user()->id;
                        $forum->desc = Input::get('description');
                        $forum->category_id = Input::get('page_category');
                        $forum->slug = Str::slug(Input::get('address'));
                        $forum->save();
                        return Redirect::to('topic/manage');
                    } else {
                        return redirect()->back()->withErrors('Address has been taken');
                    }
                } else {
                    return redirect()->back()->withErrors($v);
                }
            }
        }else{
            return 'You are banned';
        }
    }

    /**
     * Check if not banned or guest, validate all inputs,
     * update the record within the table and redirect.
     * @return string
     */
    public function updateTopic()
    {
        if(Auth::user()->group_id != 3) {
            if (!Auth::guest()) {
                $input = Input::all();
                $rules = array(
                    'page_category' => 'required',
                    'name' => 'required|max:15',
                    'address' => 'required',
                    'description' => 'max:350');
                $v = Validator::make($input, $rules);
                if ($v->passes()) {
                    $validate = Forum::where('slug', Input::get('address'))->first();
                    if (!$validate || !empty(Input::get('address'))) {
                        Forum::where('id', Input::get('forum'))->update(
                            array(
                                'name' => Input::get('name'),
                                'slug' => Input::get('address'),
                                'desc' => Input::get('description'),
                                'category_id' => Input::get('page_category'),
                            )
                        );
                    } else {
                        return redirect()->back()->withErrors('Address has been taken');
                    }
                    return redirect()->back();
                } else {
                    return redirect()->back()->withErrors($v);
                }
            }
        }else{
            return 'You are banned';
        }
    }

    /**
     * Check if not banned or guest, validate the existence of the topic,
     * delete all its associated records and redirect
     * @param $slug
     * @return string
     */
    public function deleteTopic($slug)
    {
        if(Auth::user()->group_id != 3) {
            if (!Auth::guest()) {
                $forum = Forum::where('slug', $slug)->where('user_id', Auth::user()->id)->first();

                ForumFollowing::where('forum_id', $forum->id)->delete();
                Following::where('forum_id', $forum->id)->delete();
                Reply::where('forum_id', $forum->id)->delete();
                Post::where('forum_id', $forum->id)->delete();
                Forum::find($forum->id)->delete();

                return Redirect::to('topic/manage');
            }
        }else{
            return 'You are banned';
        }
    }

//    public function listTopics(){
////        $forums = Forum::get();
//        $forums = Forum::select(DB::raw('forum.*, count(*) as `aggregate`'))
//            ->join('forum_following', 'forum.id', '=', 'forum_following.forum_id')
//            ->groupBy('forum_id')
//            ->orderBy('aggregate', 'desc')
//            ->get();
//        return View::make('main.topic.topic_list')->with('forums', $forums);
//    }
}