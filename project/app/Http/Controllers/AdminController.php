<?php namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Post;
use App\Models\Reply;
use App\Models\Feed;
use App\Models\Group;
use App\Models\Setting;
use App\Models\Forum;
use App\Models\Following;
use App\Models\ForumFollowing;
use Auth;
use Redirect;
use Response, Input, Validator;

/**
 * Class AdminController
 * @package App\Http\Controllers
 */
class AdminController extends UserController
{

    /**
     * Render the page of admin control panel
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        if ($this->isAdmin()) {
            return view('admin.template.index');
        } else {
            return Redirect::to('home');
        }
    }

    /**
     * Render the page of assign roles
     * @return $this
     */
    public function assignRole()
    {
        if ($this->isAdmin()) {
            $users = User::where('id', '>', 1)->get();
            return view('admin.user.roles')->with('users', $users);
        } else {
            return Redirect::to('home');
        }
    }

    /**
     * Display the important data in the admin control panel
     * of banned user, statistics and administrators
     * @return mixed
     */
    public function home()
    {
        if ($this->isAdmin()) {
            $users = User::get()->count();
            $posts = Post::get()->count();
            $replies = Reply::get()->count();
            $following = Following::get()->count();

            $get_admin = User::where('group_id', 1)->take(5)->get();
            $get_user = User::where('group_id', 0)->take(5)->get();
            $get_banned = User::where('group_id', 3)->take(5)->get();
            return view('admin.home.home')
                        ->with('users', $users)
                        ->with('posts', $posts)
                        ->with('replies', $replies)
                        ->with('following', $following)
                        ->with('get_admin', $get_admin)
                        ->with('get_user', $get_user)
                        ->with('get_banned', $get_banned);
        } else {
            return Redirect::to('home');
        }
    }

    /**
     * Render the settings page in the admin control panel
     * @return $this
     */
    public function settings()
    {
        if ($this->isAdmin()) {
            $settings = Setting::where('id', 1)->first();
            return view('admin.setting.setting')->with('settings', $settings);
        } else {
            return Redirect::to('home');
        }
    }

    /**
     * Render the create a user page in the admin control panel
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function createUser()
    {
        if ($this->isAdmin()) {
            return view('admin.user.create');
        } else {
            return Redirect::to('home');
        }
    }

    /**
     * Render the create a topic page in the admin control panel
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function createTopic()
    {
        if ($this->isAdmin()) {
            return view('admin.topic.create');
        } else {
            return Redirect::to('home');
        }
    }

    /**
     * Render the manage posts page in the admin control panel
     * @return $this
     */
    public function managePosts()
    {
        if ($this->isAdmin()) {
            $posts = Post::orderBy('created_at', 'DESC')->get();
            return view('admin.post.manage')->with('post', $posts);
        } else {
            return Redirect::to('home');
        }
    }

    /**
     * Render the manage users page in the admin control panel
     * @return $this
     */
    public function manageUsers()
    {
        if ($this->isAdmin()) {
            $users = User::orderBy('created_at', 'DESC')->get();
            return view('admin.user.manage')->with('users', $users);
        } else {
            return Redirect::to('home');
        }
    }

    /**
     * Render the edit post page in the admin control panel
     * @param $id
     * @return $this
     */
    public function editPost($id)
    {
        if ($this->isAdmin()) {
            $posts = Post::find($id);
            if (is_null($posts)) {
                return redirect()->route('post.index');
            }
        return view('admin.post.edit')->with('post', $posts);
        } else {
            return Redirect::to('home');
        }
    }

    /**
     * Check if admin, validate the post existence, update it and redirect
     * @return mixed
     */
    public function updatePost()
    {
        if ($this->isAdmin()) {
            $validate = Post::where('id', Input::get('post_id'))->where('user_id', Auth::user()->id)->first();
            $input = Input::all();
            $rules = array(
                'title' => 'required|min:4:|max:15',
                'body' => 'required|max:350');
            $v = Validator::make($input, $rules);
            if ($v->passes()) {
                if ($this->isAdmin()) {
                    Post::where('id', Input::get('post_id'))->update(
                        array(
                            'title' => Input::get('title'),
                            'body' => Input::get('body'),
                        )
                    );
                }
                return Redirect::to('admin/manageposts');
            } else {
                return redirect()->back()->withErrors($v);
            }
        } else {
            return Redirect::to('home');
        }
    }

    /**
     * Check if admin, validate the posts existence, delete all associated records and redirect
     * and redirect
     * @param $id
     * @return mixed
     */
    public function deletePost($id)
    {
        if ($this->isAdmin()) {
            $validate = Post::where('id', $id)->first();
                if ($validate) {
                    Post::where('id', $id)->delete();
                    Following::where('post_id', $id)->delete();
                    Reply::where('post_id', $id)->delete();
                    Feed::where('post_id', $id)->delete();
                    return Redirect::to('admin/manageposts');
                }
        } else {
            return Redirect::to('home');
        }
    }

    /**
     * Check if admin, render the page to manage topics
     * with all records arranged in a descending order
     * @return $this
     */
    public function manageTopics()
    {
        if ($this->isAdmin()) {
            $forums = Forum::orderBy('created_at', 'DESC')->get();
            return view('admin.topic.manage')->with('forums', $forums);
        }
    }

    /**
     * Check if admin, render the edit topic page with
     * the given id.
     * @param $id
     * @return $this
     */
    public function editTopic($id)
    {
        if ($this->isAdmin()) {
            $forum = Forum::find($id);
            if (is_null($forum)) {
                return redirect()->route('forums.index');
            }
            return view('admin.topic.edit')->with('forum', $forum);
        } else {
            return Redirect::to('home');
        }
    }

    /**
     * Check if admin, delete all associated records and redirect
     * @param $slug
     * @return mixed
     */
    public function deleteTopic($slug)
    {
        if ($this->isAdmin()) {
            ForumFollowing::where('forum_id', $slug)->delete();
            Following::where('forum_id', $slug)->delete();
            Reply::where('forum_id', $slug)->delete();
            Post::where('forum_id', $slug)->delete();
            Forum::find($slug)->delete();

            return Redirect::to('admin/managetopics');
        } else {
            return Redirect::to('home');
        }
    }

    /**
     * Check if admin, delete the user with the given id and redirect
     * @param $id
     * @return mixed
     */
    public function deleteUser($id)
    {
        if ($this->isAdmin()) {
            $user = User::where('id', $id)->first();
            User::where('id', $id)->delete();
            return Redirect::to('admin/manageusers');
        }
    }

    /**
     * Check if admin, render the edit user page with the given id
     * @param $id
     * @return $this
     */
    public function editUser($id)
    {
        if ($this->isAdmin()) {
            $user = User::find($id);
            if (is_null($user)) {
                return redirect()->route('forums.index');
            }
            return view('admin.user.edit')->with('user', $user);
        } else {
            return Redirect::to('home');
        }
    }

    /**
     * Check if admin, validate all inputs, update
     * the given record and redirect
     * @return mixed
     */
    public function updateUser()
    {
        if ($this->isAdmin()) {
            $input = Input::all();
            $rules = array( 'email' => 'required|email',
                'username' => 'required|max:15',
                'gender' => 'required',
                'about' => 'max:350',
                'location' => 'max:25');

            $v = Validator::make($input, $rules);
            if ($v->passes()) {
                if(Input::get('gender') == 'male' || Input::get('gender') == 'female'){
                    User::where('id', Input::get('user_id'))->update(
                        array(
                            'name' => Input::get('username'),
                            'about' => Input::get('about'),
                            'email' => Input::get('email'),
                            'gender' => Input::get('gender'),
                            'location' => Input::get('location'),
                        )
                    );
                }
                return redirect()->back();
            } else {
                return redirect()->back()->withErrors($v);
            }
        }
    }

    /**
     * Check if admin, validate all inputs, update
     * the given record and redirect
     * @return mixed
     */
    public function updateAdminSettings()
    {
        if ($this->isAdmin()) {
            $input = Input::all();
            $rules = array( 'web_title' => 'required|min:4');

            $v = Validator::make($input, $rules);
            if ($v->passes()) {
                    Setting::where('id', Input::get('setting_id'))->update(
                        array(
                            'webtitle' => Input::get('web_title'),
                            'register' => Input::get('register'),
                            'login' => Input::get('login'),
                        )
                    );
                return redirect()->back();
            } else {
                return redirect()->back()->withErrors($v);
            }
        }
    }


    /**
     * Check if admin, validate all inputs, and create a new record
     * @return mixed
     */
    public function insertUser()
    {
        if ($this->isAdmin()) {
            $input = Input::all();

            $rules = array(
                'name' => 'required|max:15|min:4',
                'email' => 'required|unique:user|email',
                'password' => 'required|min:4',
                'password_confirmation' => 'required|same:password');

            $v = Validator::make($input, $rules);
            if ($v->passes()) {
                $password = $input['password'];
                $password = bcrypt($password);

                $user = new User();
                $user->name = $input['name'];
                $user->email = $input['email'];
                $user->password = $password;
                $user->remember_token = $input['_token'];
                $user->save();

                return Redirect::to('admin/manageusers');

            } else {
                return redirect()->back()->withErrors($v);
            }
        }

    }

    /**
     * Check if admin, validate all inputs, update
     * the given record and redirect
     * @return mixed
     */
    public function updateUserRole(){
        $input = Input::all();
        $rules = array(
            'role' => 'required',
            'user' => 'required');

        $v = Validator::make($input, $rules);
        if ($v->passes()) {
            $validate_group = Group::where('group', Input::get('role'))->first();
            $validate_user = User::where('id', Input::get('user'))->first();
            if($validate_group && $validate_user)
            {
                User::where('id', Input::get('user'))->update(
                    array(
                        'group_id' => Input::get('role'),
                    )
                );
                return Redirect::to('admin/manageusers');

            }else{
                return Redirect::to('admin/manageusers');
            }

        } else {
            return redirect()->back()->withErrors($v);
        }
    }


}
