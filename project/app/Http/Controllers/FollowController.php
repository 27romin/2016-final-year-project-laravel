<?php namespace App\Http\Controllers;

use App\Models\Forum;
use App\Models\Reply;
use App\Models\Following;
use App\Models\Post;
use Illuminate\Support\Str;
use Input, Validator, Auth;
use Carbon\Carbon;
use View,Redirect;

/**
 * Class FollowController (Aka: Pinning posts)
 * @package App\Http\Controllers
 */
class FollowController extends Controller
{

    /**
     * Check if the user has pinned the post,
     * if not, then insert a new record into following.
     * This feature is for pinning posts, however its named
     * following.
     */
    public function store()
    {
        $validate = Following::where('post_id',  Input::get('post_id'))
                    ->where('user_id', Auth::user()->id)->first();
        if(!$validate){
            $input = Input::all();
            $following = new Following;
            $following->user_id = Auth::user()->id;
            $following->post_id = Input::get('post_id');
            $following->forum_id = Input::get('forum_id');
            $following->save();
        }
    }

    /**
     * An array to validate the users following. This method is
     * validated with each post to collect all pinners.
     * @param $user_id
     * @return array
     */
    public function checkFollowing($user_id)
    {
        $records[] = NULL;

        if (!Auth::guest()) {
            $followers = Following::where('user_id', $user_id)->lists('post_id');
            foreach ($followers as $follower) {
                $records[] = $follower;
            }
        }
        return $records;
    }

    /**
     * Unpin post, check if logged in, validate if the user if following.
     * If yes, then remove its record from the table
     * @return mixed
     */
    public function destroy()
    {
        if(!Auth::guest()) {
            $validate = Following::where('post_id', Input::get('post_id'))
                ->where('user_id', Auth::user()->id)->first();
            if ($validate) {
                Following::where('post_id', Input::get('post_id'))
                    ->where('user_id', Auth::user()->id)->delete();
                return redirect()->back();
            }
        }
    }
}