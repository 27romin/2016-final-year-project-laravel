<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use App\Models\User;
use App\Models\Feed;
use App\Models\ForumFollowing;
use App\Models\Following;
use App\Models\Category;
use App\Models\Group;
use App\Models\Post;
use App\Models\Forum;
use App\Models\Reply;
use Validator;
use Input;
use Redirect;
use Auth;
use Hash;
use DB;
use View;
use Image;

/**
 * Class UserController
 * @package App\Http\Controllers
 */
class UserController extends Controller
{
    /**
     * If guest, render the login page
     * @return View
     */
    public function getLogin()
    {
        if(Auth::guest()){
            return view('main.user.login');
        }
    }

    /**
     * If guest, render the registration page
     * @return View
     */
    public function getRegister()
    {
        if(Auth::guest()){
            return view('main.user.register');
        }
    }

    /**
     * If user, render the edit password page
     * @return mixed
     */
    public function getEditPassword()
    {
        if(!Auth::guest()){
            return View::make('main.user.editpassword');
        }
    }

    /**
     * If user, render the upload avatar page
     * @return mixed
     */
    public function getUploadAvatar()
    {
        if(!Auth::guest()){
            return View::make('main.user.upload_avatar');
        }
    }

    /**
     * If user, render the upload cover page
     * @return mixed
     */
    public function getUploadCover()
    {
        if(!Auth::guest()){
            return View::make('main.user.upload_cover');
        }
    }

    /**
     * If user, render the edit profile page
     * @return mixed
     */
    public function getEditProfile()
    {
        if(!Auth::guest()){
            $user = $this->findByID(Auth::user()->id);
            return View::make('main.user.editprofile')->with("user", $user);
        }
    }

    /**
     * If user and not banned, validate all
     * inputs and update the profile.
     * @return string
     */
    public function updateProfile()
    {
        if(Auth::user()->group_id != 3) {
            if (!Auth::guest()) {
                $input = Input::all();
                $rules = array('email' => 'required|email',
                    'username' => 'required|max:15',
                    'gender' => 'required',
                    'about' => 'max:350',
                    'location' => 'max:25');

                $v = Validator::make($input, $rules);
                if ($v->passes()) {
                    if (Input::get('gender') == 'male' || Input::get('gender') == 'female') {
                        User::where('id', Auth::user()->id)->update(
                            array(
                                'name' => Input::get('username'),
                                'about' => Input::get('about'),
                                'email' => Input::get('email'),
                                'gender' => Input::get('gender'),
                                'location' => Input::get('location'),
                            )
                        );
                    }
                    return redirect()->back();
                } else {
                    return redirect()->back()->withErrors($v);
                }
            }
        }else{
            return 'You are banned';
        }
    }

    /**
     * If user and not banned, validate all
     * inputs and update the password.
     * @return mixed
     */
    public function updatePassword()
    {
        if(!Auth::guest()){
            $input = Input::all();
            $rules = array( 'password' => 'required|max:15|min:6',
                            'confirm_password' => 'required|same:password');

            $v = Validator::make($input, $rules);
            if ($v->passes()) {
                    User::where('id', Auth::user()->id)->update(
                        array(
                            'password' => bcrypt(Input::get('confirm_password')),
                        )
                    );
                return $this->logout();
            } else {
                return redirect()->back()->withErrors($v);
            }
        }
    }

    /**
     * If user and not banned, validate all
     * inputs, resize the image and update the avatar.
     * @return string
     */
    public function updateAvatar()
    {
        if(Auth::user()->group != '3') {
            if (!Auth::guest()) {
                $input = Input::all();
                $rules = array('image' => 'required');

                $v = Validator::make($input, $rules);
                if ($v->passes()) {
                    $image = Input::file('image');
                    $filename = str_random(11) . $image->getClientOriginalName();
                    User::where('id', Auth::user()->id)->update(
                        array(
                            'avatar' => $filename,
                        )
                    );
                    $image->move(
                        base_path() . '/public/images/avatarupload/', $filename
                    );
                    return redirect()->back();
                } else {
                    return redirect()->back()->withErrors($v);
                }
            }
        }else{
            return 'You are banned';
        }
    }

    /**
     * If user and not banned, validate all
     * inputs, resize the image and update the cover.
     * @return mixed
     */
    public function updateCover()
    {
        if(!Auth::guest())
        {
            $input = Input::all();
            $rules = array( 'image' => 'required');

            $v = Validator::make($input, $rules);
            if ($v->passes()) {
                $image = Input::file('image');
                Image::make($image)->fit(552, 166)->save();
                $filename = str_random(11).$image->getClientOriginalName();
                    User::where('id', Auth::user()->id)->update(
                        array(
                            'cover' => $filename,
                        )
                    );
                $image->move(
                    base_path() . '/public/images/coverupload/', $filename
                );
                return redirect()->back();
            }else {
                return redirect()->back()->withErrors($v);
            }
        }
    }

    /**
     * If guest, validate all inputs and generate authentication.
     * @return mixed
     */
    public function postLogin()
    {
        if(Auth::guest())
        {
            $input = Input::all();
            $rules = array(
                'email' => 'required|email',
                'password' => 'required');

            $v = Validator::make($input, $rules);

            if ($v->passes()) {
                $credentials = array('email' => $input['email'], 'password' => $input['password']);
                if (Auth::attempt($credentials)) {
                    return Redirect::to('/home');
                } else {
                    return Redirect::to('login')->withInput()->withErrors('User not found.');
                }
            } else {
                return Redirect::to('login')->withInput()->withErrors($v);
            }
        }
    }

    /**
     * If guest, validate all inputs and save the new user as registerd.
     * @return mixed
     */
    public function postRegister()
    {
        if(Auth::guest())
        {
            $input = Input::all();

            $rules = array(
                'name' => 'required|max:15|min:4',
                'email' => 'required|unique:user|email',
                'password' => 'required|min:4',
                'password_confirmation' => 'required|same:password');

            $v = Validator::make($input, $rules);
            if ($v->passes())
            {
                $password = $input['password'];
                $password = bcrypt($password);

                $user = new User();
                $user->name = $input['name'];
                $user->email = $input['email'];
                $user->password = $password;
                $user->remember_token = $input['_token'];
                $user->save();

                return Redirect::to('login');

            } else {
                return Redirect::to('register')->withInput()->withErrors($v);
            }
        }
    }

    /**
     * Check if logged in, generate user feeds from the topics followed in a decending order
     * and render the feeds page.
     * @return mixed
     */
    public function userFeeds()
    {
        if(!Auth::guest()) {
            $posts = Feed::whereIn('forum_id', function ($query)
            {
                $forumFollowingController = new ForumFollowController();
                $query->select('forum_id')->from('forum_following')->whereIn('forum_id',  $forumFollowingController->checkForumFollowing(Auth::user()->id));

            })->orderBy('created_at', 'DESC')->get();

            $forumFollowController = new ForumFollowController();
            $followController = new FollowController();
            $forumController = new ForumController();

            $checkForumFollowing = $forumFollowController->checkForumFollowing(Auth::user()->id);
            $checkFollowing = $followController->checkFollowing(Auth::user()->id);
            $forums = $forumController->component_suggest();
            $isAdmin = $this->isAdmin();


            return View::make('main.feed.feed')
                                ->with('checkForumFollowing', $checkForumFollowing)
                                ->with('checkFollowing', $checkFollowing)
                                ->with('isAdmin', $isAdmin)
                                ->with('forums', $forums)
                                ->with('posts', $posts);
        }else{
            return Redirect::to('login');
        }
    }

    /**
     * Check if logged in, render the following page (Disabled)
     * @return mixed
     */
    public function postFollowing()
    {
        if(!Auth::guest())
        {
            $forumFollowController = new ForumFollowController();
            $forumController = new ForumController();

            $checkForumFollowing = $forumFollowController->checkForumFollowing(Auth::user()->id);
            $forums = $forumController->component_suggest();
            $isAdmin = $this->isAdmin();

            return View::make('main.following.following')
                                ->with('checkForumFollowing', $checkForumFollowing)
                                ->with('forums', $forums)
                                ->with('isAdmin', $isAdmin);
        }else {
            return Redirect::to('login');
        }
    }

    /**
     * If logged in, then log the user out
     * @return mixed
     */
    public function logout()
    {
        if(!Auth::guest()) {
            Auth::logout();
            return Redirect::to('home');
        }
    }

    /**
     * Get all users
     * @return mixed
     */
    public function findAll()
    {
        $users = User::get();
        return $users;
    }

    /**
     * Select user by id
     * @param $id
     * @return string
     */
    public function findByID($id)
    {
        $data = User::where('id', $id)->first();
        if ($data) {
            return $data;
        } else {
            return 'No user found.';
        }
    }

    /**
     * Delete user and check for its existence
     * @param $user
     * @return mixed
     */
    public function deleteUser($user)
    {
        $user = User::where('name', $user)->delete();
        if($user){
            return $user;
        }else{
            die('User not found.');
        }
    }

    /**
     * Get all users with their user groups
     * @param $group
     * @return mixed
     */
    public function getUsersByGroup($group)
    {
        $users = User::leftJoin('group', 'user.group_id', '=', 'group.group')
            ->select('group.group',
                'group.group_desc',
                'user.id',
                'user.name',
                'user.email')
            ->where('user.group_id', $group)
            ->get();

        return $users;
    }


    /**
     * Check if the user is an admin.
     * @return int
     */
    public function isAdmin()
    {
        if (!Auth::guest())
        {
            $group = new Group();
            $user = Auth::user()->group_id;
            if ($user != $group->getAdminGroup()) {
                return 0;
            } else {
                return 1;
            }
        }
    }

}
