<?php namespace App\Http\Controllers;

use App\Models\Forum;
use App\Models\Reply;
use App\Models\Following;
use App\Models\ForumFollowing;
use App\Models\Post;
use Illuminate\Support\Str;
use Input, Validator, Auth;
use Carbon\Carbon;
use View,Redirect;

/**
 * Class ForumFollowController (Aka: following topics)
 * @package App\Http\Controllers
 */
class ForumFollowController extends Controller
{

    /**
     * Check if the user is following the topic,
     * if not, then insert a new record into the table.
     *
     * @param $id
     * @return mixed
     */
    public function store($id)
    {
        $validate = ForumFollowing::where('forum_id',  $id)
                    ->where('user_id', Auth::user()->id)->first();
        $check_existance = Forum::where('id', $id)->first();
        if($check_existance){
            if(!$validate){
                $input = Input::all();
                $forumFollowing = new ForumFollowing;
                $forumFollowing->user_id = Auth::user()->id;
                $forumFollowing->forum_id = $id;
                $forumFollowing->save();
            }
        }
        return redirect()->back();
    }

    /**
     * An array to validate the users following. This method is
     * validated with each topic to collect all followers.
     * @param $user_id
     * @return array
     */
    public function checkForumFollowing($user_id)
    {
        $records[] = NULL;

        if (!Auth::guest()) {
            $forum_following =ForumFollowing::where('user_id', $user_id)->lists('forum_id');
            foreach ($forum_following as $following) {
                $records[] = $following;
            }
        }
        return $records;
    }

    /**
     * Unfollow topic, check if logged in, validate if the user if following.
     * If yes, then remove its record from the table
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        if (!Auth::guest()) {
            $validate = ForumFollowing::where('forum_id', $id)
                ->where('user_id', Auth::user()->id)->first();
            $check_existance = Forum::where('id', $id)->first();
            if ($check_existance) {
                if ($validate) {
                    ForumFollowing::where('forum_id', $id)->where('user_id', Auth::user()->id)->delete();
                    return redirect()->back();
                }
            }
        }
    }
}