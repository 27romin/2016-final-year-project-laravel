<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Models\User;


class ExampleTest extends TestCase
{
    /**
     * Testing the homepage,
     * Expects: html in the body.
     *
     * @return void
     */
//    public function test_homepage()
//    {
//        $response = $this->call('GET', '/');
//        $this->assertTrue(strpos($response->getContent(), 'html') !== false);
//    }


    /**
     * Testing the homepage when logged in,
     * Expects: html in the body.
     *
     * @return void
     */
//    public function test_home_when_loggedin()
//    {
//        $response = $this->call('GET', 'home');
//        $this->assertTrue(strpos($response->getContent(), 'html') !== false);
//    }


    /**
     * Testing the login Page,
     * Expects: Log In in the body.
     *
     * @return void
     */
//    public function test_login_pass()
//    {
//        $response = $this->call('GET', 'login');
//        $this->assertTrue(strpos($response->getContent(), 'Log In') !== false);
//    }
//

    /**
     * Testing the Register Page,
     * Expects: Sign Up in the body.
     *
     * @return void
     */
//    public function test_Register()
//    {
//        $response = $this->call('GET', 'register');
//        $this->assertTrue(strpos($response->getContent(), 'Sign Up') !== false);
//    }

    /**
     * Testing Login POST,
     * Expects: Login with data provided, and see expected page.
     *
     * @return void
     */
//    public function test_login_pass()
//    {
//        $this->visit('/login')
//            ->type('romin@live.com', 'email')
//            ->type('blank', 'password')
//            ->press('Log In')
//            ->seePageIs('/home');
//    }

    /**
     * Testing Register POST,
     * Expects: Register with data provided, and see expected page.
     * This test is expected to pass!
     *
     * @return void
     */
//    public function test_register_pass()
//    {
//        $this->visit('/register')
//            ->type('James', 'name')
//            ->type('james@live.com', 'email')
//            ->type('test123', 'password')
//            ->type('test123', 'password_confirmation')
//            ->press('Sign Up')
//            ->seePageIs('/login');
//    }

    /**
     * Testing Register POST,
     * Expects: Register with data provided, and see expected page.
     * This test is expected to fail as this user exists
     *
     * @return void
     */
//    public function test_register_fail()
//    {
//        $this->visit('/register')
//            ->type('Romin', 'name')
//            ->type('romin@live.com', 'email')
//            ->type('test123', 'password')
//            ->type('test123', 'password_confirmation')
//            ->press('Sign Up')
//            ->seePageIs('/login');
//    }


    /**
     * Testing Edit Profile POST,
     * Expects: Update all the information without fail.
     * This test is expected to pass
     *
     * @return void
     */
//    public function test_editProfile_pass()
//    {
//
//        $user = new User(array('name' => 'Romin'));
//        $this->be($user); //You are now authenticated

//        $this->visit('/profile/editprofile')
//            ->type('Romin', 'username')
//            ->type('', 'about')
//            ->type('romin@googlemail.com', 'email')
//            ->type('Male', 'gender')
//            ->type('United Kingdom', 'location')
//            ->press('Save Changes')
//            ->seePageIs('/profile/editprofile');
//    }

    /**
     * Testing Edit Profile POST,
     * Expects: Update all the information without fail.
     * This test is expected to fail
     *
     * @return void
     */
//    public function test_editProfile_fail()
//    {
//
//        $user = new User(array('name' => 'Romin'));
//        $this->be($user); //You are now authenticated
//
//
//        $this->visit('/profile/editprofile')
//            ->type('Romin', 'username')
//            ->type('testing', 'about')
//            ->type('romin@live.com', 'email')
//            ->type('Male', 'gender')
//            ->type('Afganistan', 'location')
//            ->press('Save Changes');
//    }

    /**
     * Testing Edit Password POST,
     * Expects: Update the new password without fail
     * This test is expected to Pass
     *
     * @return void
     */
//    public function test_editPassword_pass()
//    {
//        $user = new User(array('name' => 'Romin'));
//        $this->be($user); //You are now authenticated
//
//        $this->visit('/profile/editpassword')
//            ->type('test1234', 'password')
//            ->type('test1234', 'confirm_password')
//            ->press('Save Changes')
//            ->seePageIs('login');
//    }


    /**
     * Testing Edit Password POST,
     * Expects: Update the new password without fail
     * This test is expected to fail
     *
     * @return void
     */
//    public function test_editPassword_fail()
//    {

//        $user = new User(array('name' => 'Romin'));
//        $this->be($user); //You are now authenticated


//        $this->visit('/profile/editpassword')
//            ->type('', 'password')
//            ->type('test123', 'confirm_password')
//            ->press('Save Changes')
//            ->seePageIs('/login');
//    }


    /**
     * Testing Upload profile avatar,
     * Expects: Upload avatar picture without fail
     * This test is expected to pass
     *
     * @return void
     */
//    public function test_upload_avatar()
//    {

//        $user = new User(array('name' => 'Romin'));
//        $this->be($user); //You are now authenticated

//        $this->visit('/profile/avatar')
//            ->type('File Name', 'name')
//            ->attach('/images/avatarupload/', 'photo')
//            ->press('Upload')
//            ->seePageIs('/profile/avatar');
//    }


    /**
     * Testing Upload profile cover,
     * Expects: Upload cover picture without fail
     * This test is expected to pass
     *
     * @return void
     */
//    public function test_upload_cover()
//    {
//        $user = new User(array('name' => 'Romin'));
//        $this->be($user); //You are now authenticated

//        $this->visit('/profile/cover')
//            ->type('File Name', 'name')
//            ->attach('/images/coverupload/', 'photo')
//            ->press('Upload')
//            ->seePageIs('/profile/cover');
//    }


    /**
     * Testing the pinned page,
     * Expects: The user should be able to view the pinned page
     * This test is expected to pass
     *
     * @return void
     */

//    public function test_pinned()
//    {
//        $user = new User(array('name' => 'Romin'));
//        $this->be($user); //You are now authenticated
//
//        $this->visit('/pinned')
//            ->see('pinned');
//    }


    /**
     * Testing the admin page,
     * Expects: The user should be able to view the admin page
     * This test is expected to pass
     *
     * @return void
     */

//    public function test_admin()
//    {
//        $user = new User(array('name' => 'James'));
//        $this->be($user); //You are now authenticated
//
//        $this->visit('/admin')
//            ->see('Administrators');
//    }


    /**
     * Testing the admin page,
     * Expects: The user should be able to view the admin page
     * This test is expected to fail
     *
     * @return void
     */

//    public function test_admin()
//    {
//        $user = new User(array('name' => 'James'));
//        $this->be($user); //You are now authenticated
//
//        $this->visit('/admin')
//            ->see('Administrators');
//    }



}
