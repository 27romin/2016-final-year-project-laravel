$.ajaxSetup({
    headers: {'X-CSRF-Token': $('meta[name=_token]').attr('content')}
});

/* About modal */
function closeWindow() {
    $('#aboutModal').hide();
    $(document.body).css('overflow', 'auto');
}
function openAboutModal() {
    $("#aboutModal").show();
}

/* Auto refreshers */
$(document).ready(function () {
    setInterval(function () {
        $("#load_navigation").load(location.href + " #load_navigation  > *");
    }, 10000);
});

/* Toggle user menu */
function moreToggle() {
    $('.dropdown-search-container').hide();
    $('.dropdown-notification-container').hide();
    $('.notification-nav').removeClass('header-nav-active');
    $('.dropdown-more-container').toggle();
}

/* Follow Post */
function followPost(pid, fid, uid) {
    var post_id = pid;
    var forum_id = fid;
    var user_id = uid;

    $.ajax({
        type: "POST",
        url: "/follow",
        data: {post_id: post_id, forum_id: forum_id, user_id: user_id},
        cache: false,
        success: function (html) {
            $("#load_navigation").load(location.href + " #load_navigation  > *");
            $("#options_loads_" + post_id).load(location.href + " #options_loads_" + post_id);
            $("#pinners_" + post_id).load(location.href + " #pinners_" + post_id);
        }
    });
    return false;
}

/* Edit Post (Click)*/
function editPost(pid) {
    var post_id = pid;
    $('.text_wrapper' + post_id).hide();
    var data = $('.text_wrapper' + post_id).html();
    $('.edit' + post_id).show();
    $('.editbox' + post_id).html(data);
    $('.editbox' + post_id).focus();
}

/* Edit Commment (Click)*/
function editComment(cid) {
    var comment_id = cid;
    $('.text_wrapper_comment' + comment_id).hide();
    var data = $('.text_wrapper_comment' + comment_id).html();
    $('.edit_comment' + comment_id).show();
    $('.editbox_comment' + comment_id).html(data);
    $('.editbox_comment' + comment_id).focus();
}

/* Update Post */
function updatePost(pid, uid) {
    var post_id = pid;
    var user_id = uid;
    var post_title = $(".editboxtitle" + post_id).val();
    var post_value = $(".editbox" + post_id).val();
    var bbcodeParser = new BBCodeHTML();       // creates object instance of BBCodeHTML()

    $.ajax({
        type: "POST",
        url: "/updatepost",
        data: {post_id: post_id, user_id: user_id, post_title: post_title, post_value: post_value},
        cache: false,
        success: function (html) {
            $('.text_wrapper' + post_id).html(bbcodeParser.bbcodeToHtml(post_value));
            //     $('.text_wrapper' + post_id).html(post_value.replace(/Tweet\:/g, '<img src="/path/to/twitter_icon.png" alt="' + html + '" />'));
            $('#post_title_' + post_id).html(post_title);
            $('.text_wrapper' + post_id).show();
            $("#timeset_" + post_id).load(location.href + " #timeset_" + post_id + "> *");
        }
    });
}

/* Update Post */
function updateComment(cid, uid) {
    var comment_id = cid;
    var user_id = uid;
    var post_value = $(".editbox_comment" + comment_id).val();
    $('.edit_comment' + comment_id).hide();
    $.ajax({
        type: "POST",
        url: "/updatecomment",
        data: {comment_id: comment_id, user_id: user_id, post_value: post_value},
        cache: false,
        success: function (html) {
            $('.text_wrapper_comment' + comment_id).html(post_value);
            $('.text_wrapper_comment' + comment_id).show();
            $("#timeset_" + comment_id).load(location.href + " #timeset_" + comment_id + "> *");
        }
    });
}

/* Delete post */
function deletePost(pid, uid) {
    var post_id = pid;
    var user_id = uid;
    $.ajax({
        type: "POST",
        url: "/deletepost",
        data: {post_id: post_id, user_id: user_id},
        cache: false,
        success: function (html) {
            $('#story_' + post_id).slideUp();
            $("#load_navigation").load(location.href + " #load_navigation  > *");
            $("#navigation_comment_container").load(location.href + " #navigation_comment_container");
            $("#navigation_topic_container").load(location.href + " #navigation_topic_container");
        }
    });
    return false;
}

/* Delete Comment */
function deleteComment(cid, pid, uid) {
    var comment_id = cid;
    var post_id = pid;
    var user_id = uid;
    $.ajax({
        type: "POST",
        url: "/deletecomment",
        data: {comment_id: comment_id, post_id: post_id, user_id: user_id},
        cache: false,
        success: function (html) {
            $('#comment_slider_' + comment_id).slideUp();
            $("#options_loads_" + post_id).load(location.href + " #options_loads_" + post_id);
            $("#navigation_comment_container").load(location.href + " #navigation_comment_container");
        }
    });
    return false;
}

/* Unfollow Post */
function unfollowPost(pid, uid) {
    var post_id = pid;
    var user_id = uid;

    $.ajax({
        type: "POST",
        url: "/unfollow",
        data: {post_id: post_id, user_id: user_id},
        cache: false,
        success: function (html) {
            $("#navigation_pinned_container").load(location.href + " #navigation_pinned_container");
            $("#load_navigation").load(location.href + " #load_navigation  > *");
            $('#story_' + pid).slideUp();
        }
    });
    return false;
}

/* Post comment */
function postComment(text, post_id, forum_id) {
    if (event.keyCode == 13 && event.shiftKey == 0) {
        var body = text, post_id = post_id, forum_id = forum_id;
        $.ajax({
            type: "POST",
            url: "/postcomment",
            data: {body: body, post_id: post_id, forum_id: forum_id},
            cache: false,
            success: function (html) {
                $('#comment-shell' + post_id).fadeOut('slow');
                $("#loadplace" + post_id).append(html);
                $("#options_loads_" + post_id).load(location.href + " #options_loads_" + post_id);
                $("#navigation_comment_container").load(location.href + " #navigation_comment_container");
            }
        });
        return false;
    }
}