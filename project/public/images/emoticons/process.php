<?php
/**
Note: If you add a emoticon code where part of the code is itself an emoticon. For example: "0:)", where ":)" is an emoticon itself. Please make sure to add the former ["0:)" for example] above the latter [":)" for example]/
**/

// EmoFace
$emo[';p'] = 'hello.png';
$emo[':p'] = 'smaillike.png';
$emo[':D'] = 'smallopen.png';
$emo['(D'] = 'smailyes.png';
$emo['>D'] = 'facesmail.png';
$emo['([]'] = 'smillingeye.png';
$emo[':3'] = 'kiss.png';
$emo['(3'] = 'kissing.png';
$emo['(8)'] = 'woo.png';
$emo['(:8D'] = 'smailface.png';
$emo['<3)'] = 'love.png';
$emo['O()'] = 'halo.png';
$emo['8)'] = 'sungrassface.png';
$emo['/x('] = 'bandege.png';
$emo[':/'] = 'confused.png';
$emo[':.'] = 'withoutmouth.png';
$emo[':('] = 'fearful.png';
$emo['{:{'] = 'worry.png';
$emo[';('] = 'cryface.png';
$emo['TT_TT'] = 'crying.png';

// EmoAnimals
$emo['<:#'] = 'cat.png';
$emo['(>:)>'] = 'honeybee.png';
$emo['(:>'] = 'bird.png';
$emo['>:$B'] = 'dragon.png';
$emo['<;s;J'] = 'crocodie.png';

// EmoParty
$emo['O='] = 'ballroom.png';
$emo['<)$'] = 'party.png';
$emo['$o$'] = 'candy.png';
$emo['3(('] = 'birthdaycake.png';
$emo['[=]'] = 'chocolatebar.png';

// EmoPeople
$emo[':-)'] = 'person.png';
$emo[':-3'] = 'man.png';
$emo['3:-$'] = 'manxmas.png';
$emo[':-l'] = 'boy.png';
$emo['(9o'] = 'tosk.png';

$emo['<3'] = 'loveok.png';
$emo['<°>'] = 'gradulationcap.png';
$emo['{plopo}'] = 'pelofpoo.png';
$emo['{maskts}'] = 'masktarts.png';

// EmoNature
$emo['(l)'] = 'lemon.png';
$emo['(]='] = 'mashroom.png';
$emo['{hnds}'] = 'raisedhand.png';
$emo['(wrld)'] = 'earth.png';
$emo['C%='] = 'tree.png';
$emo['<%='] = 'xmastree.png';
$emo['<sun>'] = 'sun.png';
$emo['<cld>'] = 'cloud.png';
$emo['<vtge>'] = 'voltage.png';
$emo['<str>'] = 'star.png';

// EmoBike
$emo['o/o'] = 'bicycle.png';
$emo['o%o'] = 'bickman.png';

// EmoFoot
$emo['{coffe}'] = 'bevarage.png';
$emo['{cookg}'] = 'cooking.png';
$emo['{cockgrs}'] = 'cocktalgrass.png';
$emo['{hmbgr}'] = 'humburger.png';
$emo['{pizza}'] = 'pizza.png';
$emo['{tkos}'] = 'taco.png';
$emo['{meetbn}'] = 'meetonbone.png';
$emo['{hcho}'] = 'hocho.png';

// EmoTrafic
$emo['[boat]'] = 'sailboat.png';
$emo['[trin]'] = 'train.png';
$emo['[car]'] = 'racingcar.png';
$emo['[rokt]'] = 'rocket.png';
$emo['[trliht]'] = 'traficlight.png';

// EmoDisp
$emo['[kyboard]'] = 'musickeyboard.png';
$emo['[phne]'] = 'rightphone.png';
$emo['[pc]'] = 'pc.png';
$emo['[tv]'] = 'tv.png';
$emo['[gampad]'] = 'gamepad.png';
$emo['[fax]'] = 'fax.png';
$emo['[mic]'] = 'mic.png';

// EmoSport
$emo['[volybl]'] = 'volleyball.png';
$emo['[iceskt]'] = 'iceskate.png';
$emo['[crikt]'] = 'cricket.png';
$emo['[hoky]'] = 'hockey.png';

// EmoOthers
$emo['[lvelttr]'] = 'loveletter.png';
$emo['[bullht]'] = 'bulblight.png';
$emo['[alrm]'] = 'alarm.png';
$emo['[lodspek]'] = 'loudspeeker.png';
$emo['[monbg]'] = 'moneybag.png';
$emo['[bed]'] = 'bed.png';
$emo['[cardex]'] = 'cardindex.png';
$emo['[tickt]'] = 'ticket.png';
$emo['[pshpin]'] = 'pushpin.png';
$emo['[schol]'] = 'school.png';
$emo['[stat]'] = 'sigtat.png';
$emo['[alinmntr]'] = 'alienmonister.png';
$emo['[1O0]'] = '100.png';

?>