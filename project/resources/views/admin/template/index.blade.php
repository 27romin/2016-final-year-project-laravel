@include('main.include.header')
<div class="page-wrapper">
    @yield('content')
    @if (Request::is('admin/*'))
        @include('admin.include.navigation.navigation_admin')
    @endif
    <div class="float-clear"></div>
</div>
@include('main.include.footer')