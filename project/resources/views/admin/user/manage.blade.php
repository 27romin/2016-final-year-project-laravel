@extends('admin.template.index')
@section('content')
    <div class="page-margin"></div>
    <div class="float-right span74">
        <div class="list-wrapper">
            <div class="list-header">
                <div class="float-left">
                    <i class="icon-users"></i> Manage Users
                </div>
                <div class="float-clear"></div>
            </div>
            <div class="list-column">
                <table>
                    <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td width="5%">{!! $user->id !!}</td>
                            <td width="60%">{{$user->name}}</td>
                            <td width="5%"><a href="/admin/edituser/{{$user->id}}">Edit</a></td>
                            <td width="5%">
                                <button class="delete">
                                    <a href="/admin_deleteuser/{{$user->id}}"><i class="icon-trash-o"></i></a>
                                </button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection


