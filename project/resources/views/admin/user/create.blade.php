@extends('admin.template.index')
@section('content')
    <div class="page-margin"></div>
    <div class="float-right span74">
        {!! Form::open(array(  'url' => array('admin/postcreateuser'))) !!}
            <div class="form-container">
                <div class="form-header">
                    <i class="icon-user"></i> Create User
                </div>
                @if($errors->any())
                    <div class="fail-message" style="display: block;">
                        <strong>Whoops!</strong> There were some problems with your input.
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="form-input-wrapper">
                    <label class="float-left span15">
                       Username:
                    </label>
                    <div class="input float-left span80">
                        <input type="text" name="name" placeholder="Username" autocomplete="off">
                    </div>
                    <div class="float-clear"></div>
                </div>
                <div class="form-input-wrapper">
                    <label class="float-left span15">
                        Email:
                    </label>
                    <div class="input float-left span80">
                        <input type="text" name="email" placeholder="Email" autocomplete="off">
                    </div>
                    <div class="float-clear"></div>
                </div>
                <div class="form-input-wrapper">
                    <label class="float-left span15">
                        Password:
                    </label>
                    <div class="input float-left span80">
                        <input type="password" name="password"  autocomplete="off">
                    </div>
                    <div class="float-clear"></div>
                </div>
                <div class="form-input-wrapper">
                    <label class="float-left span15">
                        Confirm Password:
                    </label>
                    <div class="input float-left span80">
                        <input type="password" name="password_confirmation"  autocomplete="off">
                    </div>
                    <div class="float-clear"></div>
                </div>
                <div class="form-input-wrapper">
                    <button class="active">
                        <i class="icon-save progress-icon"></i>
                        Save Changes
                    </button>
                </div>
                {!!Form::close() !!}
            </div>
        </form>

    </div>
@endsection
