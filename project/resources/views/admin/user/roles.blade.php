@extends('admin.template.index')
@section('content')
    <div class="page-margin"></div>
    <div class="float-right span74">
        {!! Form::open(array(  'url' => array('admin_updatuserrole'))) !!}
            <div class="form-container">
                <div class="form-header">
                    <i class="icon-user"></i> User Roles
                </div>
                @if($errors->any())
                    <div class="fail-message" style="display: block;">
                        <strong>Whoops!</strong> There were some problems with your input.
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <div class="form-input-wrapper">
                    <label class="float-left span15">
                        Role:
                    </label>
                    <div class="input float-left span80">
                        <select name="role">
                            <option  value="1">
                                Administrator
                            </option>
                            <option value="0">
                                User
                            </option>
                            <option value="3">
                                Banned
                            </option>
                        </select>
                    </div>
                    <div class="float-clear"></div>
                </div>
                <div class="form-input-wrapper">
                    <label class="float-left span15">
                        User:
                    </label>
                    <div class="input float-left span80">
                        <select name="user">
                            @foreach($users as $user)
                            <option value="{{$user->id}}">
                                {{$user->name}} - {{$user->group->group_desc}}
                            </option>
                             @endforeach
                        </select>
                    </div>
                    <div class="float-clear"></div>
                </div>
                <div class="form-input-wrapper">
                    <button class="active">
                        <i class="icon-save progress-icon"></i>
                        Save Changes
                    </button>

                </div>
                {!!Form::close() !!}
            </div>
        </form>

    </div>
@endsection
