@extends('admin.template.index')
@section('content')
<div class="page-margin"></div>
<div class="float-right span74">
    {!! Form::open(array(  'url' => array('createtopic'))) !!}
        <div class="form-container">
            <div class="form-header">
                <i class="icon-lightbulb-o"></i> Create Topic
            </div>
            @if($errors->any())
                <div class="fail-message" style="display: block;">
                    <strong>Whoops!</strong> There were some problems with your input.
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="form-input-wrapper">
                <label class="float-left span25">
                    Category:
                </label>
                <div class="input float-left span65">
                    @include('main.include.category_list')
                </div>
                <div class="float-clear"></div>
            </div>
            <div class="form-input-wrapper">
                <label class="float-left span25">
                    Name:
                </label>
                <div class="input float-left span65">
                    <input type="text" name="name" placeholder="Name of your page" data-placeholder="Name of your page">
                </div>
                <div class="float-clear"></div>
            </div>
            <div class="form-input-wrapper">
                <label class="float-left span25">
                    Address:
                </label>
                <div class="input float-left span65">
                    <input type="text" name="address" data-placeholder="/webaddress" placeholder="/webaddress" autocomplete="off">
                </div>
                <div class="float-clear"></div>
            </div>
            <div class="form-input-wrapper">
                <label class="float-left span25">
                    Description:
                </label>
                <div class="input float-left span65">
                    <textarea class="auto-grow-input" name="description"  valign="top" name="page_about" placeholder="Write about your page..." data-placeholder="Write about your page..."></textarea>
                </div>
                <div class="float-clear"></div>
            </div>
            <div class="form-input-wrapper">
                <button class="active">
                    <i class="icon-flask"></i> Create
                </button>
            </div>
        </div>
    {!! Form::close() !!}
</div>
@endsection