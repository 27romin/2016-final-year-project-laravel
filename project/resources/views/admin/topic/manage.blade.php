@extends('admin.template.index')
@section('content')
    <div class="page-margin"></div>
    <div class="float-right span74">
        <div class="list-wrapper">
            <div class="list-header">
                <div class="float-left">
                    <i class="icon-file-o"></i> Manage Topics
                </div>
                <div class="float-clear"></div>
            </div>
            <div class="list-column">
                <table>
                    <tbody>
                    @foreach($forums as $forum)
                        <tr>
                            <td width="5%">{!! $forum->id !!}</td>
                            <td width="60%">{!! link_to_route('forum.show',  $forum->name, array($forum->slug)) !!}</td>
                            <td width="5%"><a href="/admin/topicedit/{{$forum->id}}">Edit</a></td>
                            <td width="5%">
                                <button class="delete">
                                    <a href="/admin_deletetopic/{{$forum->id}}"><i class="icon-trash-o"></i></a>
                                </button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection


