@extends('admin.template.index')
@section('content')
    <div class="page-margin"></div>
    <div class="float-right span74">
        {!! Form::open(array(  'url' => array('updatetopic'))) !!}
        <input type="hidden" name="gategory" value="{{$forum->category->id}}">
        <input type="hidden" name="forum" value="{{ $forum->id }}">
        <div class="form-container">
            <div class="form-header">
                <i class="icon-cog"></i> Edit Topic
            </div>
            @if($errors->any())
                <div class="fail-message" style="display: block;">
                    <strong>Whoops!</strong> There were some problems with your input.
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="form-input-wrapper">
                <label class="float-left span20">
                    Category: <br><strong>{{$forum->category->name}}</strong>
                </label>
                <div class="input float-left span70">
                    @include('main.include.category_list')
                </div>
                <div class="float-clear"></div>
            </div>
            <div class="form-input-wrapper">
                <label class="float-left span20">
                    Name:
                </label>
                <div class="input float-left span70">
                    <input type="text" name="name"  value="{{$forum->name}}" placeholder="Name of your page" data-placeholder="Name of your page">
                </div>
                <div class="float-clear"></div>
            </div>
            <div class="form-input-wrapper">
                <label class="float-left span20">
                    Address:
                </label>
                <div class="input float-left span70">
                    <input type="text" name="address" value="{{$forum->slug}}" autocomplete="off">
                </div>
                <div class="float-clear"></div>
            </div>
            <div class="form-input-wrapper">
                <label class="float-left span20">
                    Description:
                </label>
                <div class="input float-left span70">
                    <textarea class="auto-grow-input" name="description"  rows="10" valign="top" name="page_about" placeholder="Write about your page..." data-placeholder="Write about your page...">{{$forum->desc}}</textarea>
                </div>
                <div class="float-clear"></div>
            </div>
            <div class="form-input-wrapper">
                <button class="active">
                    <i class="icon-save progress-icon"></i>
                    Save Changes
                </button>

                <button class="delete">
                    <a href="/admin_deletetopic/{{$forum->id}}"><i class="icon-trash-o"></i> Remove</a>
                </button>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@endsection
