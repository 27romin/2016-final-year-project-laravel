@extends('admin.template.index')
@section('content')
    <div class="page-margin"></div>
    <div class="float-right span74">
        {!! Form::open(array(  'url' => array('admin_updatesettings'))) !!}
        <input type="hidden" name="setting_id" value="{{ $settings->id }}">
        <div class="form-container">
            <div class="form-header">
                <i class="icon-cog"></i> Website Settings
            </div>
            @if($errors->any())
                <div class="fail-message" style="display: block;">
                    <strong>Whoops!</strong> There were some problems with your input.
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="form-input-wrapper">
                <label class="float-left span15">
                    Website:
                </label>
                <div class="input float-left span80">
                    <input type="text" name="web_title" value="{{$settings->webtitle}} " autocomplete="off">
                </div>
                <div class="float-clear"></div>
            </div>
            <div class="form-input-wrapper">
                <label class="float-left span35">
                    Disable Registration:
                    {{$settings->register}}
                </label>

                <div class="input float-left span55">
                    <input type="checkbox" name="register" value="1">
                </div>

                <div class="float-clear"></div>
            </div>
            <div class="form-input-wrapper">
                <label class="float-left span35">
                    Disable Login:
                    {{$settings->login}}
                </label>

                <div class="input float-left span55">
                    <input type="checkbox" name="login" value="1" >
                </div>

                <div class="float-clear"></div>
            </div>
            <div class="form-input-wrapper">
                <button class="active">
                    <i class="icon-save progress-icon"></i>
                    Save Changes
                </button>
            </div>
            {!!Form::close() !!}
        </div>
    </div>
@endsection