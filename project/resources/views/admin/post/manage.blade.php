@extends('admin.template.index')
@section('content')
    <div class="page-margin"></div>
    <div class="float-right span74">
        <div class="list-wrapper">
            <div class="list-header">
                <div class="float-left">
                    <i class="icon-comment-o"></i> Manage Posts
                </div>
                <div class="float-clear"></div>
            </div>
            <div class="list-column">
                <table>
                    <tbody>
                    @foreach($post as $p)
                        <tr>
                            <td width="5%">{!! $p->id !!}</td>
                            <td width="60%">{!! link_to_route('post.show',  $p->title, array($p->id)) !!}</td>
                            <td width="5%"><a href="/admin/editpost/{{$p->id}}">Edit</a></td>
                            <td width="5%">
                                <button class="delete">
                                    <a href="/admin_deletepost/{{$p->id}}"><i class="icon-trash-o"></i></a>
                                </button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection


