@extends('admin.template.index')
@section('content')
    <div class="page-margin"></div>
    <div class="float-right span74">
        {!! Form::open(array(  'url' => array('admin_updatepost'))) !!}
        <input type="hidden" name="post_id" value="{{ $post->id }}">
            <div class="form-container">
                <div class="form-header">
                    <i class="icon-comment-o"></i> Edit Post
                </div>
                @if($errors->any())
                    <div class="fail-message" style="display: block;">
                        <strong>Whoops!</strong> There were some problems with your input.
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="form-input-wrapper">
                    <label class="float-left span15">
                       Post Title:
                    </label>
                    <div class="input float-left span80">
                        <input type="text" name="title" value="{{$post->title}} " autocomplete="off">
                    </div>
                    <div class="float-clear"></div>
                </div>
                <div class="form-input-wrapper">
                    <label class="float-left span15">
                        Post:
                    </label>
                    <div class="input float-left span80">
                        <textarea  name="body" rows="5" valign="top">{{$post->body}}</textarea>
                    </div>
                    <div class="float-clear"></div>
                </div>
                <div class="form-input-wrapper">
                    <button class="active">
                        <i class="icon-save progress-icon"></i>
                        Save Changes
                    </button>

                    <button class="delete">
                        <a href="/admin_deletepost/{{$post->id}}"><i class="icon-trash-o"></i> Remove</a>
                    </button>

                </div>
                {!!Form::close() !!}
            </div>
        </form>

    </div>
@endsection
