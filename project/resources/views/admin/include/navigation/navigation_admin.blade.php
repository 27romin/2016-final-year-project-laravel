<!-- Navigation topic -->
<div class="float-left span25">
    <div class="list-wrapper">
        <a class="list-column column-hover" href="/home" align="center">
            {{$app->settings->webtitle}}
        </a>
    </div>
    <div class="list-wrapper page-settings">
        <div class="list-header">
            Website
        </div>

        <a class="list-column column-hover" href="/admin/settings/" >
            <i class="icon-cog"></i>  Settings
        </a>
    </div>
    <div class="list-wrapper page-settings">
        <div class="list-header">
            Users
        </div>
        <a class="list-column column-hover" href="/admin/createuser/" >
            <i class="icon-user"></i> Create User
        </a>
        <a class="list-column column-hover" href="/admin/manageusers/" >
            <i class="icon-users"></i> Manage Users
        </a>
        <a class="list-column column-hover" href="/admin/userroles/" >
            <i class="icon-users"></i> User Roles
        </a>
    </div>

    <div class="list-wrapper page-settings">
        <div class="list-header">
            Posts
        </div>
        <a class="list-column column-hover" href="/admin/manageposts/" >
            <i class="icon-comment-o"></i> Manage Posts
        </a>
    </div>

    <div class="list-wrapper page-settings">
        <div class="list-header">
            Topics
        </div>
        <a class="list-column column-hover"  href="/admin/createtopic/">
            <i class="icon-file-o"></i> Create Topic
        </a>
        <a class="list-column column-hover" href="/admin/managetopics/">
            <i class="icon-file-o"></i> Manage Topics
        </a>
    </div>
</div>
<div class="clearfix">&nbsp;</div>
