@extends('admin.template.index')
@section('content')
    <div class="page-margin"></div>
    <div class="float-right span74">
        <div class="list-wrapper">
            <div class="list-header">
                <div class="float-left">
                    {{$app->settings->webtitle}} Stats
                </div>
                <div class="float-clear"></div>
            </div>
            <div class="list-column">
                <table>
                    <tbody>
                    <tr>
                        <td width="40%">Total Members: </td>
                        <td width="60%">{{$users}}</td>
                    </tr>
                    <tr>
                        <td width="40%">Total Posts: </td>
                        <td width="60%">{{$posts}}</td>
                    </tr>
                    <tr>
                        <td width="40%">Total Comments: </td>
                        <td width="60%">{{$replies}}</td>
                    </tr>
                    <tr>
                        <td width="40%">Pins: </td>
                        <td width="60%">{{$following}}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>


        <div class="list-wrapper">
            <div class="list-header">
                <div class="float-left">
                    Administrators
                </div>
                <div class="float-clear"></div>
            </div>
            <div class="list-column">
                <table>
                    <tbody>
                    @foreach($get_admin as $admin)
                        <tr>
                            <td width="5%">{{$admin->id}}</td>
                            <td width="60%">{{$admin->name}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>



        <div class="list-wrapper">
            <div class="list-header">
                <div class="float-left">
                    Users
                </div>
                <div class="float-clear"></div>
            </div>
            <div class="list-column">
                <table>
                    <tbody>
                    @foreach($get_user as $user)
                    <tr>
                        <td width="5%">{{$user->id}}</td>
                        <td width="60%">{{$user->name}}</td>
                        <td width="5%"><a href="/admin/edituser/{{$user->id}}">Edit</a></td>
                        <td width="5%">
                            <button class="delete">
                                <a href="/admin_deleteuser/{{$user->id}}"><i class="icon-trash-o"></i></a>
                            </button>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <div class="list-wrapper">
            <div class="list-header">
                <div class="float-left">
                    <i class="icon-ban"></i> Banned
                </div>
                <div class="float-clear"></div>
            </div>
            <div class="list-column">
                <table>
                    <tbody>
                    @foreach($get_banned as $banned)
                        <tr>
                            <td width="5%">{{$banned->id}}</td>
                            <td width="60%">{{$banned->name}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>


    </div>
@endsection
