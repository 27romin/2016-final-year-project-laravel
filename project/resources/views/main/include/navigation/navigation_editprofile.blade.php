<!-- Navigation edit profile -->
<div class="float-left span25">
    <div class="list-wrapper">
        <div class="list-header">
            Menu
        </div>
        <a class="list-column" href="/profile/editprofile">
            <i class="icon icon-cog"></i> General Settings
        </a>
        <a class="list-column" href="/profile/avatar">
            <i class="icon icon-camera"></i> Timeline Avatar
        </a>
        <a class="list-column" href="/profile/cover">
            <i class="icon icon-image"></i> Timeline Cover
        </a>
        <a class="list-column" href="/profile/editpassword">
            <i class="icon icon-lock"></i> Password
        </a>
    </div>
</div>