<!-- Navigation Create Topic -->
<div class="float-right span40">
    <div class="list-wrapper">
        <div class="list-header">
            <i class="icon-info-circle"></i> Starting a topic?
        </div>
        <div class="list-column">
            <strong class="bold-500">
                Connect with a wide range of people
            </strong>
            <br>
            - Think of a unique topic, a topic that users would love to follow
        </div>
        <div class="list-column">
            <strong class="bold-500">
                Increase your followers
            </strong>
            <br>
            - Share your topic with friends, social media platforms and start associating with popular topics!
        </div>
        <div class="list-column">
            <strong class="bold-500">
                Post intresting content
            </strong>
            <br>
            - Post new updates so people who cares knows what's going on and what matters.
        </div>
        <div class="list-column">
            <strong class="bold-500">
                Make your topic look great
            </strong>
            <br>
            - Create or upload a vibrant cover/display picture to make your topic look good
        </div>
    </div>
</div>