<!-- Navigation topic -->
<div class="float-left span25">
    <div class="list-wrapper">
        <a class="list-column column-hover" href="/forum/{{$forum->slug}}" align="center">
            {{$forum->name}}
        </a>
    </div>
    <div class="list-wrapper page-settings">
        <div class="list-header">
            Menu
        </div>

        <a class="list-column column-hover" href="/managetopic/settings/{{$forum->slug}}" >
            <i class="icon-cog"></i> General Settings
        </a>

        <a class="list-column" href="/managetopic/avatar/{{$forum->slug}}">
            <i class="icon icon-camera"></i> Topic Avatar
        </a>
        <a class="list-column" href="/managetopic/cover/{{$forum->slug}}">
            <i class="icon icon-image"></i> Topic Cover
        </a>
    </div>
</div>
<div class="clearfix">&nbsp;</div>
