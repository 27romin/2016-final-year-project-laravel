<!-- Navigation when in a topic -->
@include('main.include.modal.modal_about')
<div class="float-left span35">
    @include('main.include.component.component_forum_card')
    @include('main.include.component.component_forum_buttons')
    @include('main.include.component.component_forum_info')
    @include('main.include.component.component_suggest')
</div>
<div class="clearfix">&nbsp;</div>