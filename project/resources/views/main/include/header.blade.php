<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <title>{{$app->settings->webtitle}}</title>
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta name="_token" content="{!! csrf_token() !!}"/>
    <link href="{{ asset('/css/themes/mk2/style.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/themes/mk2/style-1.1.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/themes/mk2/style-1.2.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/themes/mk2/font-awesome/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/themes/mk2/animate.css') }}" rel="stylesheet">
    <!--[if IE 7]>
    <link href="http://minimalkit.esy.es//themes/mk2/css/font-awesome/font-awesome-ie7.css" rel="stylesheet">
    <![endif]-->
    <script src="{{ asset('scripts/javascript/jquery.1.10.2.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('scripts/javascript/jquery-ui-1.10.4.custom.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('scripts/javascript/jquery.ui.touch-punch.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('scripts/javascript/jquery-timeago.js') }}" type="text/javascript"></script>
    <script src="{{ asset('scripts/javascript/jquery.form.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('scripts/javascript/script.js') }}" type="text/javascript"></script>
</head>
<body data-rewrite="1">
<!-- Header -->
<div class="header-wrapper" name="top">
    <div class="header-content">
        <div class="float-left">
            <table border="0" cellspacing="0" cellpadding="0">
                <tbody>
                <tr>
                    <td class="header-site-logo" align="left" valign="top">
                        <a href="/home">
                            <i class="icon-comment"></i><span>{{$app->settings->webtitle}}</span>
                        </a>
                    </td>
                    <td align="left" valign="top">
                        {{--<div class="header-live-search">--}}
                            {{--<input value="Search for people, trends, pages and groups" placeholder="Search for people, trends, pages and groups" data-placeholder="Search for people, trends, pages and groups" type="text" onkeyup="SK_headerSearch(this.value);">--}}
                        {{--</div>--}}
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        @if (!Auth::guest())
            <div class="float-right" id="load_navigation">
                {{--<a class="notification-nav header-nav float-left" href="#" title="Notifications">--}}
                    {{--<i class="icon icon-bell-o"></i>--}}
                    {{--<span class="new-update-alert">0</span>--}}
                {{--</a>--}}
                <a class="header-nav float-left" href="/home"  style="font-variant: small-caps;">
                    <i class="icon icon-home"></i>
                    {{--<span class="new-update-alert">0</span>--}}
                </a>
                {{--<a class="header-nav float-left" href="/topiclist"  style="font-variant: small-caps;">--}}
                        {{--popular--}}
                    {{--<span class="new-update-alert">0</span>--}}
                {{--</a>--}}
                <a class="followers-nav header-nav float-left " href="/pinned" title="Pinned">
                    <i class="icon icon-thumb-tack"></i>
                    @if(Auth::user()->following->count() > 0)
                        <span class="new-update-alert">{{Auth::user()->following->count()}}</span>
                    @endif
                </a>

                <div class="header-user-info float-left cursor-hand" style="position: relative;">
                    <a class="header-user-link" href="#">
                        <img class="header-user-avatar" src="{{url('/')}}{{avatar_dir}}{{ Auth::user()->avatar}}" width="30px">
                        <span>{{ Auth::user()->name}}</span>
                    </a>
                    <i class="icon-angle-down dropdown-icon"></i>
                    <span class="dropdown-overlay-wrap" onclick="moreToggle();"></span>
                </div>
                <div class="float-clear"></div>
            </div>
        @endif
        <div class="float-clear"></div>
    </div>
</div>

<!-- User Navigation Toggle -->
<div class="dropdown-more-container" style="display: none;">
    <div class="dropdown-more-wrapper">
        <div class="dropdown-more-content float-right">
            <div class="more-list-wrapper">
                @if(Auth::user())
                @if(Auth::user()->group_id == 1)
                <a class="more-list" href="/admin/home">
                     Administration
                </a>
                @endif
                @endif
                <a class="more-list" href="/topic/manage">
                    <i class="icon icon-cubes"></i> Manage Topic
                </a>
                <a class="more-list" href="/topic/create">
                    <i class="icon icon-lightbulb-o"></i>Create Topic
                </a>
                <a class="more-list" href="/profile/editprofile">
                    <i class="icon icon-cogs"></i> Edit Profile
                </a>
                <a class="more-list" <a href="{{ url('/logout') }}">
                    <i class="icon icon-lock"></i> Log Out
                </a>
            </div>
        </div>
        <div class="float-clear"></div>
    </div>
</div>

