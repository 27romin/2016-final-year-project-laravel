<script src="{{ asset('scripts/javascript/bbcode.js') }}" type="text/javascript"></script>
<script src="{{ asset('scripts/javascript/tgscript.js') }}" type="text/javascript"></script>

<!-- Footer -->
<div class="footer-wrapper">
    <div class="footer-content" align="center">
        <div class="footer-line">
            <a href="#">
                About
            </a> -
            <a href="#">
                Contact
            </a> -

            <a href="#">
                Privacy Policy
            </a> -

            <a href="#">
                Terms of Use
            </a> -

            <a href="#">
                Disclaimer
            </a>
        </div>

        <div class="footer-line">
            Topic © 2016.
        </div>
    </div>
</div>
</body></html>