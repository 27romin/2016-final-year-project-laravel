<!-- Componenet profile information -->
<div class="timeline-sidebar">
    @if(Auth::user()->about)
    <div class="sidebar-div">
        <table border="0" width="100%" cellspacing="0" cellpadding="0">
            <tbody>
            <tr>
                <td width="25px" align="left" valign="top">
                    <a href="/profile/editprofile">
                        <i class="icon icon-comment-o" alt="Edit about me" title="Edit about me"></i>
                    </a>
                </td>
                <td align="left" valign="top">
                    {!! nl2br(e(Auth::user()->about)) !!}
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    @endif
    @if(Auth::user()->location)
    <div class="sidebar-div">
        <table border="0" width="100%" cellspacing="0" cellpadding="0">
            <tbody>
            <tr>
                <td width="25px" align="left" valign="top">
                    <a href="/profile/editprofile">
                        <i class="icon icon-home" alt="Location" title="Location"></i>
                    </a>
                </td>
                <td align="left" valign="top">
                    Location: {!! Auth::user()->location !!}
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    @endif
    <div class="sidebar-div">
        <table border="0" width="100%" cellspacing="0" cellpadding="0">
            <tbody>
            <tr>
                <td width="25px" align="left" valign="top">
                    <a href="/profile/editprofile">
                        <i class="icon icon-envelope" alt="Email Address" title="Email Address"></i>
                    </a>
                </td>
                <td align="left" valign="top">
                   {!! Auth::user()->email !!}
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    @if(Auth::user()->gender)
    <div class="sidebar-div">
        <table border="0" width="100%" cellspacing="0" cellpadding="0">
            <tbody>
            <tr>
                <td width="25px" align="left" valign="top">
                    <a href="/profile/editprofile">
                        <i class="icon icon-user" alt="Edit about me" title="Edit about me"></i>
                    </a>
                </td>
                <td align="left" valign="top">
                   {!! ucfirst(Auth::user()->gender) !!}
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    @endif
</div>

<div class="timeline-sidebar">
    <div class="sidebar-div">
        <table border="0" width="100%" cellspacing="0" cellpadding="0">
            <tbody>
            <tr>
                <td width="25px" align="left" valign="top">
                    <i class="icon-thumb-tack"></i>
                </td>
                <td align="left" valign="top">
                    <span id="navigation_pinned_container">
                        Pinned: {{count(Auth::user()->following)}}
                    </span>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="sidebar-div">
        <table border="0" width="100%" cellspacing="0" cellpadding="0">
            <tbody>
            <tr>
                <td width="25px" align="left" valign="top">
                    <i class="icon-file-o"></i>
                </td>
                <td align="left" valign="top">
                    <span id="navigation_topic_container">
                    My Posts: {{count(Auth::user()->post)}}
                    </span>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>