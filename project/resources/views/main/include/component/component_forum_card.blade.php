<!-- Component profile card -->
<div class="short-profile-wrapper">
    <div class="short-cover">
        <img src="{{url('/')}}{{forum_cover_dir}}{{$forum->cover}}" width="100%">
        <a class="short-avatar" href="#">
            <img src="{{url('/')}}{{forum_avatar_dir}}{{$forum->avatar}}" width="70px">
        </a>
    </div>
    <div class="short-info">
        <ul>
            <li>
                <a class="short-name bold-500" href="#">
                    {{ $forum->name}}
                </a>
                <br> {{ '@'.$forum->user->name}} &bull; {{count($forum->forumFollowers)}} Followers
            </li>
        </ul>
    </div>
</div>
