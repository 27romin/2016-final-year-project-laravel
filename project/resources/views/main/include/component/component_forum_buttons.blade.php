<!-- Componenet forum button -->
<div class="timeline-buttons">
    <table border="0" width="100%" cellspacing="0" cellpadding="0">
        <tbody>
        <tr>
            @if (!Auth::guest())
            <td align="center" valign="middle">
                @if (!in_array($forum->id, $checkForumFollowing))
                        <a class="active btn-lke-act swing animated" href="/followtopic/{{$forum->id }}">
                            <i data-icon="check" class="icon-plus"></i> Follow
                        </a>
                @else
                    <a class="inactive fadeIn animated" href="/unfollowtopic/{{$forum->id }}">
                        <i data-icon="check" class="icon-minus"></i> Unfollow
                    </a>
                @endif
            </td>
            @endif
            {{--<td align="center" valign="middle">--}}
                {{--<a class="active fadeIn animated" href="#">--}}
                    {{--<i class="icon-star"></i>--}}
                    {{--Associated (99)--}}
                {{--</a>--}}
            {{--</td>--}}
            <td align="center" valign="middle">
                <a class="active fadeIn animated" onclick="openAboutModal();">
                    <i class="icon-bullhorn" ></i>
                    About
                </a>
            </td>
        </tr>
        </tbody>
    </table>
</div>

