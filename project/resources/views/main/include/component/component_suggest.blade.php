<!-- Componenet suggestions -->
<div class="list-wrapper suggestions-wrapper">
    <div class="list-header">
        <div class="float-left">
            <i class="icon-star"></i> Suggested Topics
        </div>
        <div class="float-clear"></div>
    </div>
    <div class="suggestions-content">
        @foreach($forums as $forums)
            <div class="list-column">
                <table border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tbody>
                    <tr>
                        <td width="50px" align="left" valign="middle">
                            <a href="/forum/{!! $forums->slug !!}">
                                <img  style="max-height: 40px; max-width: 40px;  overflow:hidden;"  src="{{url('/')}}{{forum_avatar_dir}}{{$forums->avatar}}">
                            </a>
                        </td>
                        <td width="55%" align="left" valign="middle">
                            <a class="bold-500" href="/forum/{!! $forums->slug !!}">
                                {!! $forums->name !!}
                            </a>
                            <div class="info">
                                <small>{!! str_limit($forums->desc, 30); !!}</small>
                            </div>
                        </td>
                        <td class="column-btn" align="right" valign="middle">
                            @if(!Auth::guest())
                            @if (!in_array($forums->id, $checkForumFollowing))
                                <a class=" swing animated" href="/followtopic/{{$forums->id }}">
                                    <i data-icon="check" class="icon-plus"></i> Follow
                                </a>
                            @else
                                <a class="btn-addr fadeIn animated" href="/unfollowtopic/{{$forums->id }}">
                                    <i data-icon="check" class="icon-minus"></i> Unfollow
                                </a>
                            @endif
                            @endif
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        @endforeach
    </div>
</div>