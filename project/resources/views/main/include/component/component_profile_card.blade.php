<!-- Componenet profile card -->
<div class="short-profile-wrapper">
    <div class="short-cover">
        <img src="{{url('/')}}{{cover_dir}}{{Auth::user()->cover}}" width="100%" style="">
        <a class="short-avatar" href="#">
            <img src="{{url('/')}}{{avatar_dir}}{{Auth::user()->avatar}}" width="70px">
        </a>
    </div>
    <div class="short-info">
        <ul>
            <li>
                <a class="short-name bold-500" href="#">
                    {{ Auth::user()->name}}
                </a>
                <br> {{ '@'.Auth::user()->name}}
            </li>
        </ul>
    </div>
</div>
