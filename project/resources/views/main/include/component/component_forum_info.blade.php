<!-- Componenet forum information -->
<div class="timeline-sidebar">
    <div class="sidebar-div">
        <table border="0" width="100%" cellspacing="0" cellpadding="0">
            <tbody>
            <tr>
                <td width="25px" align="left" valign="top">
                    <i class="icon-list"></i>
                </td>
                <td align="left" valign="top">
                    Category: {{$forum->category->name}}
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="sidebar-div">
        <table border="0" width="100%" cellspacing="0" cellpadding="0">
            <tbody>
            <tr>
                <td width="25px" align="left" valign="top">
                    <i class="icon-comment-o"></i>
                </td>
                <td align="left" valign="top">
                       {{$forum->desc}}
                </td>
            </tr>
            </tbody>
        </table>
    </div>

</div>

<div class="timeline-sidebar">
    {{--<div class="sidebar-div">--}}
        {{--<table border="0" width="100%" cellspacing="0" cellpadding="0">--}}
            {{--<tbody>--}}
            {{--<tr>--}}
                {{--<td width="25px" align="left" valign="top">--}}
                    {{--<i class="icon-exclamation"></i>--}}
                {{--</td>--}}

                {{--<td align="left" valign="top">--}}
                        {{--Followers: {{count($forum->forumFollowers)}}--}}
                {{--</td>--}}
            {{--</tr>--}}
            {{--</tbody>--}}
        {{--</table>--}}
    {{--</div>--}}
    <div class="sidebar-div">
        <table border="0" width="100%" cellspacing="0" cellpadding="0">
            <tbody>
            <tr>
                <td width="25px" align="left" valign="top">
                    <i class="icon-file-o"></i>
                </td>
                <td align="left" valign="top">
                    <span id="navigation_topic_container">
                        Topics: {{count($forum->posts)}}
                    </span>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="sidebar-div">
        <table border="0" width="100%" cellspacing="0" cellpadding="0">
            <tbody>
            <tr>
                <td width="25px" align="left" valign="top">
                    <i class="icon-comments-o"></i>
                </td>

                <td align="left" valign="top">
                    <span id="navigation_comment_container">
                        Comments: {{$forum->repliesCount()}}
                    </span>
                </td>
            </tr>
            </tbody>
        </table>
    </div>

</div>