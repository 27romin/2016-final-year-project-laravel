<script>
    function closeEditPost(pid) {
        var post_id = pid;
        $('#editPostModal'+post_id).hide();
        $(document.body).css('overflow','auto');
    }

    function editPostModal(pid){
        var post_id = pid;
        $("#editPostModal"+post_id).show();
    }
</script>
<!-- Componenet modal edit post -->
@if(!Auth::guest())
<div id="editPostModal{{$post->id}}" style="display:none">
    <div class="window-container">
        <div class="window-background" onclick="closeEditPost({{$post->id}});"></div>
        <div class="window-wrapper fadeIn animated">
            <div class="window-header-wrapper">
                <i class="icon-minus-sign"></i>
                Edit Post
            </div>
            <div class="window-content-wrapper">
                <div class="window-list-wrapper">
                    <input class="editboxtitle{{$post->id}}" type="text" style=" width:97%" value="{{$post->title}}"><p></p>
                    <textarea class="editbox{{$post->id}}" style="resize: both; overflow: auto; height: 50px; width:97%">{{$post->body}}</textarea>
                </div>
            </div>
            <div class="button-content-wrapper">
                <button class="active" onclick="updatePost({{$post->id}}, {{Auth::user()->id}}); closeEditPost({{$post->id}});">
                    Edit
                </button>
            </div>
        </div>
    </div>
</div>
    @endif

