<!-- Componenet modal about -->
<div id="aboutModal" style="display:none">
    <div class="window-container">
        <div class="window-background" onclick="closeWindow();"></div>
        <div class="window-wrapper zoomInUp animated">
            <div class="window-header-wrapper">
                <i class="icon-minus-sign"></i>
                About: {{ $forum->name}}
            </div>
            <div class="window-content-wrapper">
                <div class="window-list-wrapper">
                    {{ $forum->desc}}
                </div>
            </div>
            <div class="button-content-wrapper">
                <button class="active" onclick="closeWindow();">
                    ok
                </button>
            </div>
        </div>
    </div>
</div>