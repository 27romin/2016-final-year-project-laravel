<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <title>{{$app->settings->webtitle}}</title>
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta name="_token" content="{!! csrf_token() !!}"/>
    <link href="{{ asset('/css/themes/mk2/style.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/themes/mk2/style-1.1.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/themes/mk2/style-1.2.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/themes/mk2/welcome.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/themes/mk2/font-awesome/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/themes/mk2/animate.css') }}" rel="stylesheet">
    <!--[if IE 7]>
    <link href="http://minimalkit.esy.es//themes/mk2/css/font-awesome/font-awesome-ie7.css" rel="stylesheet">
    <![endif]-->
    <script src="{{ asset('scripts/javascript/jquery.1.10.2.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('scripts/javascript/jquery-ui-1.10.4.custom.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('scripts/javascript/jquery.ui.touch-punch.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('scripts/javascript/jquery-timeago.js') }}" type="text/javascript"></script>
    <script src="{{ asset('scripts/javascript/jquery.form.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('scripts/javascript/script.js') }}" type="text/javascript"></script>
</head>
<body data-rewrite="1">
<!-- Header -->
<div class="header-wrapper" name="top">
    <div class="header-content">
        <div class="float-left">
            <table border="0" cellspacing="0" cellpadding="0">
                <tbody>
                <tr>
                    <td class="header-site-logo" align="left" valign="top">
                        <a href="home">
                            <i class="icon-comment"></i><span>Topic</span>
                        </a>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="float-clear"></div>
    </div>
</div>

<!-- Main Page -->
@if (Auth::guest())
<div class="page-wrapper">
    <div class="page-margin"></div>
    <div class="float-left span60 desktop-only">
    </div>
    <div class="float-right span35">
            {!! Form::open(array(   'url' => array('login'), 'class' => 'welcome-form login-form')) !!}
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
        @if($errors->any())
            <div class="fail-message">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
            <div class="form-header">
                <div class="float-left">
                    Log In
                </div>
                <div class="float-right">
                    <a href="/register">
                        Sign Up
                    </a>
                </div>
                <div class="float-clear"></div>
            </div>
            <div class="form-content">
                @if ($app->settings->login == 0)
                <div class="input-wrapper">
                    {!! Form::text('email', '', ['id' => 'inputEmail', 'placeholder' => 'Email Address']) !!}
                </div>
                <div class="input-wrapper">
                    <input name="password" type="password"  placeholder="Password" >
                </div>
                <button class="submit-btn active"><i class="icon-signin progress-icon"></i> Log In</button>
                @else
                    Login is disabled.
                @endif
                <div class="input-wrapper">
                    <div class="float-right">
                        <a href="/forgot-password">
                            Forgot Password?
                        </a>
                    </div>
                    <div class="float-clear"></div>
                </div>
            </div>
        {!! Form::close() !!}
        <div class="fb-button">
            <a href="#">
                <i class="icon-facebook"></i>
                <span>Connect with Facebook</span>
            </a>
        </div>
    </div>
    <div class="float-clear"></div>
</div>
@else
    Redirect::to('user/login');
    @endif
@include('main.include.footer')