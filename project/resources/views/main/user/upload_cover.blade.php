@extends('main.template.index')
@section('content')
<div class="page-margin"></div>
<!-- Conent Seperation -->
<div class="float-right span74">
        {!! Form::open(array(  'url' => 'updatecover', 'files' => true)) !!}
    {{--{{ Form::open(array('url' => 'updateavatar', 'files' => true, 'method' => 'post')) }}--}}

        <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="form-container">
                <div class="form-header">
                    <i class="icon icon-image"></i> Change cover
                </div>
                @if($errors->any())
                    <div class="fail-message" style="display: block;">
                        <strong>Whoops!</strong> There were some problems with your input.
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div align="center">
                    <img src="{{url('/')}}{{cover_dir}}{{Auth::user()->cover}}" width="100%"  height="auto">
                </div>
                <div class="form-input-wrapper">
                    <table border="0" width="100%">
                        <tbody><tr>
                            <td align="left" valign="middle">
                                <input type="file" name="image" accept="image/jpeg,image/png" style="width: 85%;">
                                <button>
                                    Upload
                                </button>
                            </td>
                        </tr>
                        </tbody></table>
                </div>
            </div>
        {!! Form::close() !!}
    </div>
@endsection