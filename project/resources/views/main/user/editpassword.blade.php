@extends('main.template.index')
@section('content')
<div class="page-margin"></div>
<!-- Conent Seperation -->
<div class="float-right span74">
        {!! Form::open(array(  'url' => array('updatepassword'))) !!}
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="form-container">
                <div class="form-header">
                    <i class="icon icon-lock"></i> Edit Password
                </div>
                @if($errors->any())
                    <div class="fail-message" style="display: block;">
                        <strong>Whoops!</strong> There were some problems with your input.
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="form-input-wrapper">
                    <label class="float-left span15">
                        New Password:
                    </label>
                    <div class="input float-left span80">
                        <input type="text" name="password">
                    </div>
                    <div class="float-clear"></div>
                </div>
                <div class="form-input-wrapper">
                    <label class="float-left span15">
                        Confirm Password:
                    </label>
                    <div class="input float-left span80">
                        <input type="text" name="confirm_password">
                    </div>
                    <div class="float-clear"></div>
                </div>

                <div class="form-input-wrapper">
                    <button class="active">
                        <i class="icon-save progress-icon"></i>
                        Save Changes
                    </button>
                </div>
            </div>
        {!! Form::close() !!}
    </div>
@endsection