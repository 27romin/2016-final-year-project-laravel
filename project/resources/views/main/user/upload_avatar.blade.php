@extends('main.template.index')
@section('content')
<div class="page-margin"></div>
<!-- Conent Seperation -->
<div class="float-right span74">
        {!! Form::open(array(  'url' => 'updateavatar', 'files' => true)) !!}
    {{--{{ Form::open(array('url' => 'updateavatar', 'files' => true, 'method' => 'post')) }}--}}
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="form-container">
                <div class="form-header">
                    <i class="icon icon-camera"></i> Change avatar
                </div>
                @if($errors->any())
                    <div class="fail-message" style="display: block;">
                        <strong>Whoops!</strong> There were some problems with your input.
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="form-input-wrapper">
                    <table border="0" width="95%">
                        <tbody><tr>
                            <td align="left" valign="middle">
                                <img class="radius3" src="{{url('/')}}{{avatar_dir}}{{Auth::user()->avatar}}" width="48px" height="auto" alt="Romin Patel">
                            </td>

                            <td align="left" valign="middle">
                                <input type="file" name="image" accept="image/jpeg,image/png" style="width: 80%;">
                                <button>
                                    Upload
                                </button>
                            </td>
                        </tr>
                        </tbody></table>
                </div>
            </div>
        {!! Form::close() !!}
    </div>
@endsection