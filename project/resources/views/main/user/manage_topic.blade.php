@extends('main.template.index')
@section('content')
    <div class="page-margin"></div>
    <div class="float-left span59">
        <div class="list-wrapper">
            <div class="list-header">
                <div class="float-left">
                    <i class="icon-flag item-mpge"></i> Topics You Manage
                </div>
                <div class="float-right">
                    <a class="hover-line" href="/topic/create">
                        Create Topic
                    </a>
                </div>
                <div class="float-clear"></div>
            </div>
            @if($errors->any())
                <div class="form-container">
                <div class="fail-message" style="display: block;">
                    <strong>Whoops!</strong> There were some problems with your input.
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                </div>
            @endif
            @foreach($forums as $forum)
            <div class="list-column">
                <table border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tbody><tr>
                        <td width="62px" align="left" valign="middle">
                            <a href="/forum/{{$forum->slug}}">
                                <img  style="max-height: 40px; max-width: 40px;  overflow:hidden;"  src="{{url('/')}}{{forum_avatar_dir}}{{$forum->avatar}}">
                            </a>
                        </td>
                        <td align="left" valign="middle">
                            <a class="bold-500" href="/managetopic/settings/{{$forum->slug}}">
                                {{$forum->name}}
                            </a><br>
                            <small>{{$forum->desc}}</small>
                        </td>
                    </tr>
                    </tbody></table>
            </div>
                @endforeach
        </div>
    </div>
@endsection


