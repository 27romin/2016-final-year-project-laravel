@extends('main.template.index')
@section('content')
    <div class="page-margin"></div>
    <div class="float-right span63">
        <div class="stories-container">
            <div class="stories-wrapper">
                @forelse ($posts as $photo)
                    @foreach($photo->post as $post)
                        @include('main.include.modal.modal_edit_post')
                        <div id="story_{{$post->id}}" class="story-wrapper story_{{$post->id}}">
                            <div class="publisher-content-wrapper">
                                @include('main.post.post_user_card')
                                @include('main.post.post_header')
                                @include('main.post.post_content')
                                @include('main.post.post_option')
                            </div>
                            @include('main.post.post_comment_box')
                            @include('main.post.post_pinner')
                        </div>
                        <script>
                            // Popover
                            var delay = 800, timer, time = 400, timeout;
                            $(".publisher-wrapper .name_{{$post->id}}, .short-profile_{{$post->id}}").mouseover(function () {
                                        timer = setTimeout(function () {
                                            $('.short-profile_{{$post->id}}').stop(true).css('opacity', 1).show();
                                        }, delay);
                                    })
                                    .mouseout(function () {
                                        clearTimeout(timer);
                                        timeout = setTimeout(function () {
                                            var isHover = $('.short-profile_{{$post->id}}').is(":hover");
                                            if (isHover !== true) {
                                                $('.short-profile_{{$post->id}}').fadeOut(200);
                                            }
                                        }, time);
                                        return false;
                                    });
                        </script>
                    @endforeach
                @empty
                    <div class="story-wrapper">
                        <div class="publisher-content-wrapper">
                            <div class="publisher-wrapper" align="center">
                                <em>Follow topics from the left hand navigation for regular feeds</em>
                            </div>
                        </div>
                    </div>
                @endforelse
            </div>
        </div>
    </div>
@endsection
