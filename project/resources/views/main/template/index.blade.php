@include('main.include.header')
<div class="page-wrapper">
    @yield('content')
    @if (Request::is('pinned') || Request::is('home'))
        @include('main.include.navigation.navigation_pinned')
    @endif
    @if (Request::is('profile/*'))
        @include('main.include.navigation.navigation_editprofile')
    @endif
    @if (Request::is('forum/*'))
        @include('main.include.navigation.navigation_forum')
    @endif
    @if (Request::is('topic/*'))
        @include('main.include.navigation.navigation_create_topic')
    @endif
    @if (Request::is('managetopic/*'))
        @include('main.include.navigation.navigation_manage_topic')
    @endif
    <div class="float-clear"></div>
</div>
@include('main.include.footer')