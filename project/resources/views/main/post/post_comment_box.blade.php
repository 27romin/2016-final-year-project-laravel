<!-- Post comment box -->
<div class="comments-container hidden" style="display: hidden;">
    {!! Form::open() !!}
    <div class="comments-wrapper">
        @include('main.comment.comment')
    </div>
    @if (!Auth::guest())
        <div id="comment-shell{{$post->id}}" class="comment-wrapper comment-publisher-box">
            <table border="0" width="100%" cellspacing="0" cellpadding="0">
                <tbody>
                <tr>
                    <td align="left" valign="middle">
                        <div  class="comment-textarea">
                            <textarea  name="text" placeholder="Write a comment... Press Enter to post" data-placeholder="Write a comment... Press Enter to post" data-height="24" onkeyup="postComment(this.value,{{$post->id}},{{$post->forum->id}});">Write a comment... Press Enter to post</textarea>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    @endif
    {!! Form::close() !!}
</div>