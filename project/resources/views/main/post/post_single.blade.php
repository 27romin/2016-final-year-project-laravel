@extends('main.template.index')
@section('content')
<div class="page-margin"></div>
<!-- Content body -->
<div class="float-right span63">
<!-- Render single post -->
<div class="stories-container">
    <div class="stories-wrapper">
        @include('main.include.modal.modal_edit_post')
        <div id="story_{{$post->id}}" class="story-wrapper story_{{$post->id}}">
            <div class="publisher-content-wrapper">
                <!-- Post Card -->
                @include('main.post.post_user_card')
                <!-- Post Header -->
                @include('main.post.post_header')
                <!-- Post Body-->
                @include('main.post.post_content')
                <!-- Post Options -->
                @include('main.post.post_option')
            </div>
            <!-- Post Comments form-->
            @include('main.post.post_comment_box')
            <!-- Post Pinners-->
            @include('main.post.post_pinner')
        </div>
        <script>
            // Popover
            var delay = 800, timer, time = 400, timeout;
            $(".publisher-wrapper .name_{{$post->id}}, .short-profile_{{$post->id}}").mouseover(function () {
                timer = setTimeout(function () {
                    $('.short-profile_{{$post->id}}').stop(true).css('opacity', 1).show();
                }, delay);
            })
               .mouseout(function ()
               {
                   clearTimeout(timer);
                   timeout = setTimeout(function () {
                       var isHover = $('.short-profile_{{$post->id}}').is(":hover");
                       if (isHover !== true)
                       {
                           $('.short-profile_{{$post->id}}').fadeOut(200);
                       }
                   }, time);
                   return false;
               });
        </script>
    </div>
</div>
    </div>
@endsection