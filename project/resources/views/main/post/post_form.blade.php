@if(Auth::user())
<!-- Display errors -->
@if($errors->any())
    <a href="#" class="close" data-dismiss="alert">&times;</a>
    {!! implode('', $errors->all('<li class="error">:message</li>')) !!}
    @endif
<!-- Post form -->
    <div class="story-publisher-box">
        {!! Form::open(array('route' => 'post.store')) !!}
        {!! Form::hidden('forum_id', $forum->id, array('id' => 'forum_id')) !!}
        <div class="inputs-container-textarea">
            <table border="0" width="100%" cellspacing="0" cellpadding="0">
                <tbody>
                <tr>
                    <td width="65px" align="left" valign="top">
                        <a href="#">
                            <img class="avatar" src="{{url('/')}}{{avatar_dir}}{{Auth::user()->avatar}}" rel="tooltip" data-placement="top" title="{{Auth::user()->name}}" width="54px" height="54px">
                        </a>
                    </td>
                    <td align="left" valign="middle">
                        <div class="inputs-textarea">
                            <div class="input-wrapper input-wrapper" style="display: block;">
                                <table border="0" width="100%" cellspacing="0" cellpadding="0">
                                    <tbody>
                                    <tr>
                                        <td align="left" valign="top">
                                            <div class="inputs-textarea">
                                                {!! Form::text('title', '', array('id' => 'title', 'required' => true, 'class' => 'auto-grow-input ', 'placeholder' => 'Write a short description of the subject...')) !!}
                                            </div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            {!! Form::textarea('body', '', array('id' => 'body', 'required' => true, 'class' => 'bbtextarea auto-grow-input',  'placeholder' => '... Remember, make it interesting, post here when ready.')) !!}
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>

        <div class="inputs-container" >
            <div class="input-wrapper emoticons-wrapper">
                <img src="{{url('/')}}{{emoticons}}hello.png" width="25px" onclick="wrapText('bbtextarea', ';', 'P');">
                <img src="{{url('/')}}{{emoticons}}smaillike.png" width="25px" onclick="wrapText('bbtextarea', ':', 'P');">
                <img src="{{url('/')}}{{emoticons}}smallopen.png" width="25px" onclick="wrapText('bbtextarea', ':', 'D');">
                <img src="{{url('/')}}{{emoticons}}smailyes.png" width="25px" onclick="wrapText('bbtextarea', 'K', 'D');">
                <img src="{{url('/')}}{{emoticons}}bandege.png" width="25px" onclick="wrapText('bbtextarea', 'X', 'C');">
                <img src="{{url('/')}}{{emoticons}}crying.png" width="25px" onclick="wrapText('bbtextarea', 'TT_', 'TT');">
                <img src="{{url('/')}}{{emoticons}}love.png" width="25px" onclick="wrapText('bbtextarea', ':in', 'love');">
                <img src="{{url('/')}}{{emoticons}}sungrassface.png" width="25px" onclick="wrapText('bbtextarea', ':sungrass', 'face');">
                <img src="{{url('/')}}{{emoticons}}confused.png" width="25px" onclick="wrapText('bbtextarea', ':', 'confused');">
                <img src="{{url('/')}}{{emoticons}}smailface.png" width="25px" onclick="wrapText('bbtextarea', ':', 'smailface');">
                <img src="{{url('/')}}{{emoticons}}facesmail.png" width="25px" onclick="wrapText('bbtextarea', ':', 'facesmail');">
                <img src="{{url('/')}}{{emoticons}}smillingeye.png" width="25px" onclick="wrapText('bbtextarea', ':', 'smillingeye');">
                <img src="{{url('/')}}{{emoticons}}halo.png" width="25px" onclick="wrapText('bbtextarea', ':', 'halo');">
                <img src="{{url('/')}}{{emoticons}}woo.png" width="25px" onclick="wrapText('bbtextarea', ':', 'woo');">
            </div>
        </div>

        <script>
            function wrapText(elementID, openTag, closeTag) {
                var textArea = $('.' + elementID);
                var len = textArea.val().length;
                var start = textArea[0].selectionStart;
                var end = textArea[0].selectionEnd;
                var selectedText = textArea.val().substring(start, end);
                var replacement = openTag + selectedText + closeTag;
                textArea.val(textArea.val().substring(0, start) + replacement + textArea.val().substring(end, len));
            }
        </script>

        <!-- Options and post it-->
        <div class="more-wrapper">
            <table border="0" width="100%" cellspacing="0" cellpadding="0">
                <tbody>
                <tr>
                    <td width="28px" align="center" valign="middle">
                    <span class="option" onclick="wrapText('bbtextarea', '[img]', '[/img]');" rel="tooltip" data-placement="top" title="" data-original-title="Photos">
                        <i class="icon-image"></i>
                    </span>
                    </td>
                    <td width="28px" align="center" valign="middle">
                    <span class="option" onclick="toggleMediaGroup('.story-publisher-box .emoticons-wrapper');" rel="tooltip" data-placement="top" title="" data-original-title="Icons">
                        <i class="icon-smile-o"></i>
                    </span>
                    </td>
                    <td width="28px" align="center" valign="middle">
                    <span class="option" onclick="wrapText('bbtextarea', '[b]', '[/b]');" rel="tooltip" data-placement="top" title="" data-original-title="Photos">
                        <i class="icon-bold"></i>
                    </span>
                    </td>
                    <td width="28px" align="center" valign="middle">
                    <span class="option" onclick="wrapText('bbtextarea', '[i]', '[/i]');" rel="tooltip" data-placement="top" title="" data-original-title="Photos">
                        <i class="icon-italic"></i>
                    </span>
                    </td>
                    <td width="28px" align="center" valign="middle">
                    <span class="option" onclick="wrapText('bbtextarea', '[u]', '[/u]');" rel="tooltip" data-placement="top" title="" data-original-title="Photos">
                        <i class="icon-underline"></i>
                    </span>
                    </td>

                    <td align="right" valign="middle">
                        <button class="submit-btn active" name="story_submit_btn">
                            <i class="icon-edit progress-icon"></i><span> Post</span>
                        </button>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <script>
            function toggleMediaGroup(chosen_input_selector) {
                input_wrapper = $(chosen_input_selector);
                group_id = input_wrapper.attr('data-group');
                if (input_wrapper.css('display') == "none") {
                    $('.input-wrapper[data-group=' + group_id + ']')
                            .slideUp()
                            .find('input').val('').show()
                            .end()
                            .find('.result-container').remove()
                            .end()
                            .find('.remove-btn').remove();
                    input_wrapper.slideDown();
                } else {
                    $('.input-wrapper[data-group=' + group_id + ']').slideUp();
                }
            }

        </script>
        {!! Form::close() !!}
    </div>
@endif

