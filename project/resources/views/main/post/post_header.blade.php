<!-- Post header -->
<div class="publisher-wrapper">
    <table border="0" width="100%" cellspacing="0" cellpadding="0">
        <tbody>
        <tr>
            <td width="75px" align="left" valign="top">
                <a href="#">
                    <img class="avatar bounceIn animated" src="{{url('/')}}{{avatar_dir}}{{ $post->user->avatar}}" rel="tooltip" data-placement="top" title="" width="64px" height="64px" alt="{!! $post->user->name !!}" data-original-title="{!! $post->user->name !!}">
                </a>
            </td>
            <td align="left" valign="middle">
                <div id="post_title_{{$post->id}}">
                    <a href="/post/{{$post->id}}">
                        {!! str_limit($post->title, 50); !!}
                    </a>
                </div>
                <a class="name name_{{$post->id}}" href="#">
                    {!! $post->user->name !!}
                </a>
            </td>

            @if (!Auth::guest())
                @if(Auth::user()->name == $post->user->name || $isAdmin)
                    <td class="ctrl-btn" align="right" valign="middle">
                        <div class="setting-buttons">
                            <i class="icon-angle-down setting_menu_{{$post->id}} cursor-hand" rel="tooltip" data-placement="top" title="" data-original-title="Settings"></i>
                        </div>
                        <ul class="dropdown-setting-container setting_item_{{$post->id}} fadeInDown animated" style="display:none;">
                            <li>
                                <span class="remove-btn cursor-hand" title="Remove">
                                    <div class="action" onclick="deletePost({{$post->id}}, {{Auth::user()->id}});">
                                        <i class="icon-trash-o progress-icon"></i> Remove
                                    </div>
                                    <div class="action-desc">Delete this publication</div>
                                </span>
                            </li>
                        </ul>
                        <script>
                            $(".setting_menu_{{$post->id}}").click(function () {
                                $(".setting_item_{{$post->id}}").toggle();
                            });
                        </script>
                    </td>
                @endif
            @endif
        </tr>
        </tbody>
    </table>
</div>