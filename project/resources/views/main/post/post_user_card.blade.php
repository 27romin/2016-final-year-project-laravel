<!-- User post card-->
<div class="popover top short-profile_{{$post->id}} animation-expandUp" style="display:none;">
    <div class="arrow"></div>
    <div class="popover-profile-wrapper">
        <div class="popover-cover">
            <img src="{{url('/')}}{{cover_dir}}{{$post->user->cover}}" width="100%">
            <a class="popover-avatar fadeIn animated" href="#">
                <img src="{{url('/')}}{{avatar_dir}}{{$post->user->avatar}}" width="70px">
            </a>
        </div>
        <div class="popover-info">
            <a class="popover-name bold-500" href="#">
                {!! $post->user->name !!}
            </a><br> {!! '@'.$post->user->name !!}
        </div>
    </div>
</div>

