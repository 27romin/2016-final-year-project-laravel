<!-- Options of post-->
<div class="options-wrapper">
    <div class="float-left">
        <div id="timeset_{{$post->id}}">
            <span class="time-wrapper ajax-time" title="{!! $post->updated_at !!}">about a minute ago</span>
        </div>
    </div>
    <div class="float-right" id="options_loads_{{$post->id}}">
        <abbr class="space4"></abbr>
        @if (!Auth::guest())
            @if(Auth::user()->name == $post->user->name || $isAdmin)
                <span class="edit_link" onclick="editPostModal({{$post->id}});">
                    <i class="icon-edit" title="Edit" alt="Edit"></i>
                </span>
            @endif
        @endif
        <abbr class="space4"></abbr>
        <span class="story-comment-btn opt" onclick="javascript:$('#story_{{$post->id}} .comments-container').slideToggle();" title="Comments">
            <i class="icon-comments-o progress-icon" data-icon="comments-o"></i>
        </span>
        <span class="story-comment-activity">
            <span class="comment-activity activity-btn" onclick="javascript:$('#story_{{$post->id}} .comments-container').slideToggle();" title="Comments">
                {{$post->replies()->count()}}c
            </span>
        </span>
        <abbr class="space4"></abbr>
        @if (Request::is('pinned'))
        <span class="story-share-btn opt" title="Unpin">
            <i class="icon-times-circle-o" onclick="unfollowPost({{$post->id}}, {{Auth::user()->id}})"></i>
        </span>
        @endif

        @if (Request::is('home') || Request::is('forum/*') || Request::is('post/*'))
        @if (!in_array($post->id, $checkFollowing))
                @if (!Auth::guest())
                <span class="story-share-btn opt" title="Pin">
                    <i class="icon icon-thumb-tack" onclick="followPost({{$post->id}}, {{$post->forum->id}}, {{Auth::user()->id}})"></i>
                </span>
                @endif
        @else
            <span title="Pinned">
                <i class="icon-check-circle-o"></i>
            </span>
        @endif
        @endif
        <span class="story-share-activity">
            <span class="" onclick="javascript:$('#story_{{$post->id}} .pinners-wrapper').slideToggle();" title="Pinners">
                @if (count($post->followers) > 0)
                    {{count($post->followers).'p'}}
                    @else
                    0p
                @endif
            </span>
        </span>
    </div>
    <div class="float-clear"></div>
</div>