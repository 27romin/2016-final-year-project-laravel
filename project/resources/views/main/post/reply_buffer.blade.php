<!-- [B] Single comment -->
<div id="comment_slider_{{$reply->id}}">
<div class="comment-wrapper">
    <table border="0" width="100%" cellspacing="0" cellpadding="0" class="commentTable">
        <tbody>
        <tr>
            <td width="60px" align="left" valign="top">
                <a href="#">
                    <img class="avatar" src="{{url('/')}}{{avatar_dir}}{{ $reply->user->avatar}}" width="50px" height="50px">
                </a>
            </td>
            <td align="left" valign="top">
                <div class="comment-content">
                    <a class="name" href="#">
                        {!! $reply->user->name !!}
                    </a>
                    <div class="setting-buttons">
                        @if(Auth::user()->name == $reply->user->name )
                            <span class="comment-edit-btn cursor-hand" onclick="editComment({{$reply->id}});" title="Edit">
                            <i class="icon-edit edit_link"></i>
                        </span>
                            <span class="comment-remove-btn cursor-hand" onclick="deleteComment({{$reply->id}}, {{$reply->post->id}}, {{$reply->user->id}});" title="Remove">
                            <i class="icon-remove progress-icon"></i>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="comment-text">
                    <div class="text_wrapper_comment{{$reply->id}}">{!! $reply->body !!}</div>
                    <div class="edit_comment{{$reply->id}}" style="display:none">
                        <textarea class="editbox_comment{{$reply->id}}" onchange="updateComment({{$reply->id}}, {{Auth::user()->id}});" style="resize: both; overflow: auto; height: 30px; width:98%"></textarea>
                    </div>
                </div>
                <script>
                    $(document).mouseup(function()
                    {
                        $('.edit_comment{{$reply->id}}').hide();
                        $('.text_wrapper_comment{{$reply->id}}').show();
                    });

                    $(".editbox_comment{{$reply->id}}").mouseup(function()
                    {
                        return false
                    });
                </script>

                <div class="other-data">
                    <div id="timeset_{{$reply->id}}">
                    <span class="time ajax-time" title="{!! $reply->created_at !!}">about a minute ago</span>
                    </div>
                </div>
            </td>
        </tr>
        </tbody>
    </table>
</div>
</div>

