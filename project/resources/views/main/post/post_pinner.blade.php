<!-- Followers-->
<div class="pinners-wrapper" style="display: none;">
    <div id="pinners_{{$post->id}}">
        @if (count($post->followers) > 0)
            @foreach ($post->followers as $followers)
                <img class="avatar avatar bounceIn animated" src="{{url('/')}}{{avatar_dir}}{{$followers->avatar}}"  title="{{$followers->name}}" width="30px"  alt="{{$followers->name}}" >
            @endforeach
        @else
            --
        @endif
    </div>
</div>