@extends('main.template.index')
@section('content')
<div class="page-margin"></div>
@foreach($forums as $forum)
<div class="short-profile-wrapper span25 float-left">
    <div class="short-cover">
        <img src="{{url('/')}}{{forum_cover_dir}}{{$forum->cover}}" width="100%">
        <a class="short-avatar" href="#">
            <img src="{{url('/')}}{{forum_avatar_dir}}{{$forum->avatar}}" width="70px">
        </a>
    </div>
    <div class="short-info">
        <ul>
            <li>
                <a class="short-name bold-500" href="/forum/{{ $forum->slug}}">
                   {!! str_limit($forum->name, 18); !!}
                </a>
                <br> {{count($forum->forumFollowers)}} Followers
    </li>
</ul>
</div>
</div>
@endforeach
<div class="clearfix">&nbsp;</div>
<!-- Componenet suggestions -->
{{--<div class="list-wrapper suggestions-wrapper">--}}
    {{--<div class="list-header">--}}
        {{--<div class="float-left">--}}
            {{--<i class="icon-fire" alt="Hot Topics" title="Hot Topics"></i> Hot Topics--}}
        {{--</div>--}}
        {{--<div class="float-clear"></div>--}}
    {{--</div>--}}
    {{--<div class="suggestions-content">--}}
        {{--@foreach($forums as $forum)--}}
            {{--<div class="list-column">--}}
                {{--<table border="0" width="100%" cellspacing="0" cellpadding="0">--}}
                    {{--<tbody>--}}
                    {{--<tr>--}}
                        {{--<td width="75px" align="left" valign="middle">--}}
                            {{--<a href="/forum/">--}}
                                {{--<img width="65px" height=65px" src="../../images/default-page-avatar.png">--}}
                            {{--</a>--}}
                        {{--</td>--}}
                        {{--<td width="45%" align="left" valign="middle">--}}
                            {{--<a class="bold-500" href="/forum/{{ $forum->slug}}">--}}
                                {{--{{ $forum->name}}</a>  <br>{{'@'.$forum->category->name}}--}}
                             {{--<br>--}}
                            {{--<small >--}}
                                {{--{{ $forum->desc}}--}}
                            {{--</small>--}}
                        {{--</td>--}}


                        {{--<td class="column-btn" align="right" valign="middle">--}}
                            {{--<a class="btn-add">--}}
                                {{--<i data-icon="like" class="icon-plus progress-icon"></i>--}}
                                {{--<span>Follow</span>--}}

                            {{--</a><p></p>--}}
                            {{--<small>Comments: {{$forum->repliesCount()}}  Posts: {{count($forum->posts)}}</small>--}}
                        {{--</td>--}}
                    {{--</tr>--}}
                    {{--</tbody>--}}
                {{--</table>--}}
            {{--</div>--}}
{{--@endforeach--}}
    {{--</div>--}}
{{--</div>--}}



@endsection