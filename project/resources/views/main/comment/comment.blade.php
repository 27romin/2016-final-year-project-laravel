<!-- Limit Comments -->
@if(count($post->replies) > 3)
    <div class="view-more-wrapper" align="center">
        <div class="view_comments_{{$post->id}}"></div>
    </div>
@endif

<!-- Render comments -->
<div id="comments_container_{{$post->id}}">
@foreach($post->replies as $reply)
<div id="comment_slider_{{$reply->id}}">
<div class="comment-wrapper comment_{{$post->id}}">
    <table border="0" width="100%" cellspacing="0" cellpadding="0" class="commentTable">
        <tbody>
        <tr>
            <td width="60px" align="left" valign="top">
                <a href="#">
                    <img class="avatar avatar bounceIn animated" src="{{url('/')}}{{avatar_dir}}{{$reply->user->avatar}}" width="50px" height="50px">
                </a>
            </td>
            <td align="left" valign="top">
                <div class="comment-content">
                    <a class="name" href="#">
                        {!! $reply->user->name !!}
                    </a>
                    <div class="setting-buttons">
                        @if (!Auth::guest())
                        @if(Auth::user()->name == $reply->user->name || $isAdmin)
                        <span class="comment-edit-btn cursor-hand" onclick="editComment({{$reply->id}});" title="Edit">
                            <i class="icon-edit edit_link"></i>
                        </span>
                        <span class="comment-remove-btn cursor-hand" onclick="deleteComment({{$reply->id}}, {{$post->id}}, {{$reply->user->id}});" title="Remove">
                            <i class="icon-remove"></i>
                        </span>
                        @endif
                        @endif
                    </div>
                </div>

                <!-- Comment content -->
                <div class="comment-text">
                    <div class="text_wrapper_comment{{$reply->id}}">{!! $reply->body !!}</div>
                    <div class="edit_comment{{$reply->id}}" style="display:none">
                        @if(Auth::user())
                        <textarea class="editbox_comment{{$reply->id}}" onchange="updateComment({{$reply->id}}, {{Auth::user()->id}});" style="resize: both; overflow: auto; height: 30px; width:98%"></textarea>
                        @endif
                    </div>
                </div>

                <script>
                    $(document).mouseup(function() {
                        $('.edit_comment{{$reply->id}}').hide();
                        $('.text_wrapper_comment{{$reply->id}}').show();
                    });

                    $(".editbox_comment{{$reply->id}}").mouseup(function() {
                        return false
                    });
                </script>

                <div class="other-data">
                    <div id="timeset_{{$reply->id}}">
                    <span class="time ajax-time" title="{!! $reply->updated_at !!}">about a minute ago</span>
                    </div>
                </div>
            </td>
        </tr>
        </tbody>
    </table>
</div>
</div>
@endforeach
</div>

<!-- Render fresh comment -->
<div id="loadplace{{$post->id}}"></div>

<script>
    /* Toggle Comments*/
    var comments = 4;
    togglecomments = "- Show less comments";
    showcomments = "+ View all comments";

    $(".view_comments_{{$post->id}}").html( showcomments );
    $(".comment_{{$post->id}}:not(:gt(-"+comments+"))").hide();

    $(".view_comments_{{$post->id}}").click(function (e) {
        e.preventDefault();
        if ($(".comment_{{$post->id}}:eq(-"+comments+")").is(":hidden")) {
            $(".comment_{{$post->id}}:hidden").show();
            $(".view_comments_{{$post->id}}").html( togglecomments );
        } else {
            $(".comment_{{$post->id}}:not(:gt(-"+comments+"))").hide();
            $(".view_comments_{{$post->id}}").html(  showcomments );
        }
    });
</script>

